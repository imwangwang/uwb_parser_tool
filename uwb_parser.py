import matplotlib.pyplot as plt
import numpy as np
import re

# Function to parse log data
def parse_log_file(file_path):
    # Regular expression pattern to extract data
    # pattern = r"uwbpos stable:([\d\.]+),.*x:([\d\.-]+), y:([\d\.-]+), z:([\d\.-]+),.*timestamp:(\d+)"
    pattern = r"uwbpos (stable|not stable):([\d\.]+),.*x:([\d\.-]+), y:([\d\.-]+), z:([\d\.-]+),.*timestamp:(\d+)"
    timestamps = []
    distances = []
    positions = []

    # Reading from the log file
    with open(file_path, 'r') as file:
        for line in file:
            match = re.search(pattern, line)
            if match:
                #print(match.groups())
                stable, distance, x, y, z, timestamp = match.groups()
                timestamps.append(int(timestamp))
                distances.append(float(distance))
                positions.append((float(x), float(y), float(z)))

    return timestamps, distances, np.array(positions)

# Plotting function
def plot_data(positions, distances):
    fig, ax = plt.subplots()
    
    # Scatter plot of positions
    ax.scatter(positions[:, 0], positions[:, 2], c='blue', label='ARCore positions')

    # Draw circles for UWB distances
    for pos, dist in zip(positions, distances):
        circle = plt.Circle((pos[0], pos[2]), dist, color='red', fill=False, linewidth=1, linestyle='--')
        #circle = plt.Circle((pos[0], pos[2]), dist, color='green', fill=False)
        ax.add_artist(circle)

    # Set uniform axis ranges
    ax.set_xlim(-7.0, 7.0)
    ax.set_ylim(-7.0, 7.0)
    ax.set_aspect('equal')

    ax.set_xlabel('X position (m)')
    ax.set_ylabel('Y position (m)')
    ax.set_title('ARCore Positions and UWB Distances')

    plt.legend()
    plt.show()

# Plotting function for 3D data
def plot_data_3d(positions, distances):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    # 3D scatter plot of positions
    scatter = ax.scatter(positions[:, 0], positions[:, 1], positions[:, 2], c='blue', label='ARCore positions')

    # Drawing spheres for UWB distances
    for pos, dist in zip(positions, distances):
        # Create a sphere at position `pos` with radius `dist`
        u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
        x = dist * np.cos(u) * np.sin(v) + pos[0]
        y = dist * np.sin(u) * np.sin(v) + pos[1]
        z = dist * np.cos(v) + pos[2]
        ax.plot_wireframe(x, y, z, color="r", alpha=0.01)

    # Set uniform axis ranges
    ax.set_xlim(-7, 7)
    ax.set_ylim(-7, 7)    
    ax.set_zlim(-7, 7)    

    ax.set_xlabel('X position (m)')
    ax.set_ylabel('Y position (m)')
    ax.set_zlabel('Z position (m)')
    ax.set_title('ARCore Positions and UWB Distances in 3D')
    plt.legend()
    plt.show()

# Example usage
file_path = './issue2/aplogd/test1.log'  # Change this to your actual log file path
timestamps, distances, positions = parse_log_file(file_path)
#print(positions)
#print(distances)
plot_data_3d(positions, distances)
#plot_data(positions, distances)
