# UWB Qorvo Tools

Welcome to the 'uwb-qorvo-tools' repo (uqt).  
This repo is gathering tools that may be used by customers to operate Qorvo's UWB devices.

The provided tools are relying on Qorvo's uwb-uci libraries 
[uwb-uci](https://gitlab.com/qorvo/uwb-mobile/tools/uci/uwb-uci/-/tree/uwb-qorvo-tools-main).  
Please note that uwb-qorvo-tools uses `uwb-qorvo-tools-main` branch as a mian branch (not `main`). (uwb-uci will go to its own repo at one time or another :-)   
uwb-uci is available as a sub-module of this repo.  

This readme stands as an internal user / developer / maintainer guide.  

The next document to read (referenced here and there) is [README-customer](README-customer.md) 
which stands as a user manual from a customer standpoint. 
It describes several practical uses-cases.  


[[_TOC_]]


## Source Tree


```
uwb-qorvo-tools
    ├── README.md                 <-- Qorvo internal only
    ├── README-customer.md
    ├── requirements.txt          <-- python3 install requirements
    ├── uqt_env                   <-- initial setup to source
    ├── bin                       <-- script folder
    │   ├── fp
    │   ├── gen_aoalut
    │   ├── uqt_ls
    │   ├── uqt_release
    │   ├── uqt_lsvariants
    │   ├── run_fira_test_periodic_tx
    │   ├── ...
    │   └── uqt_info
    ├── lib-ext
    │   |── uwb-uci                  <--- UCI transport layer libraries
    │        ├── README.md
    │        ├── requirements.txt
    │        ├── test
    │        │   ├── __init__.py
    │        │   ├── test_core.py
    │        │   ├── test_transport.py
    │        │   └── test_v1_0.py
    │        └── uci
    │           ├── __init__.py
    │           ├── fira.py         <--- uci from fira std
    │           ├── qorvo.py        <--- Qorvo vendor uci customization
    │           ├── addin_se.py     <--- Specific uci customization
    │           ├── addin_ccc.py
    │           ├── transport.py
    │           └──...
    |           ...
    └── Qorvo_license.txt
    └── utils
        └── qm35_poll               <--- used for adb UCI transport
```

## Getting the Source

### Submodules

You should not forget to update the submodules at each new fetching and/or branch change.

### Initial clone

```
# Clone the repo:
    git clone git@gitlab.com:qorvo/uwb-mobile/tools/uci/uwb-qorvo-tools.git
# Checkout the master branch:
    cd uwb-qorvo-tools 
    git checkout main
# Update the sub-modules to the required version:
    git submodule update --recursive --init
```

### Further updates

```
# Update your main:
    git pull
# Update your sub-modules:
    git submodule update
```
## Qorvo Internal User Manual

### General Prerequisites

Please refer to [README-customer](README-customer.md#prerequisites)


### General Quick Start

Please refer to [README-customer](README-customer.md#quick-start)


### Selecting the proper variant

Working from this repo, UQT may be configured for a given variant.  
A 'variant' is a list of addins and a (customer) customization.  
As of 2023.04:  
 - available customization: qorvo, purple, radar, SD2002, customer, android
 - several addins available: addin_se, addin_ccc, addin_post_tx_tone

By default, the qorvo customization is used and all addins are selected.
You may change this setting using scripts and environment variables as described below.
```
# list available variants:
    uqt_lsvariants
# list the variant in use:
    uqt_info
# Select another customization:
    export UQT_CUSTOM=purple
# Down-size your lib to a list of addins by
  selecting a list of addin (comma separated list):
    export UQT_ADDINS='addin_se:addin_ccc'
```
The 'qorvo' customization gives you access to all functionalities WHEN POSSIBLE.
Incompatible library or scripts functionalities main be tracked by watching files names.
If 2 files have the same name and a different suffix ( __\<color> or  __\<qorvo> for instance), only one will be used depending on the UQT_CUSTOM value.

As off 2023.04.27, you still have to type the __\<color> script suffix, even when a \<color> customization is set.

### Additional Information

Please refer to [README-customer](README-customer.md#manual)   
Further information is available in Qorvo Application Manuals or FIRA standards from the `/doc` folder.

## Maintainer Guide

### Repo and branch Organization

For memory: python based tooling is split into 2 git repositories:

- uwb-uci: Ultrawideband UCI Library, the base library dealing with uci transports.
- uwb-qorvo-tools: Ultrawideband Qorvo Tools, the uci commands available as scripts and delivered to customers.

uwb-uci is a submodule of uwb-qorvo-tools

### Package versioning

There is no internal release process. This repository operates in continuous integration mode. In means `main` branch always contains the newest version of this repo. For customer release process see below.

### Customer release process

#### Creating customer release tag

Package versioning is relying on the git tags. 
The expected tag format is:

`release/<customer>/v<X.Y.Z>-RC<N>`, where   
`<customer>` is a one of UQT flavor,   
`X` = major, `Y` = minor, `Z` = patch,, `W` = Release Candidate number   
`-RC<W>` is not required when we tag final release,not a Release Candidate

Example tag names:   
`release/QM35SDK/v0.1.0-RC1`   
`release/pink/v0.1.5-RC2`   
`release/purple/v1.0.0`   
`release/blue/v12.43.4`   

#### Creating customer release package

After you create release tag push it on the server. CI pipeline will prepare package for you. For more details you can check `.gitlab-ci.yml` file and read [release README](tools/release/README.md)

#### Modifying a customer package

If some changes in customer package is required provide them, commit changes, create a new release tag and push it to the git server. New package will be generated on CI.

#### Scripts (non-regression) Testing

Several test scripts are available to verify uqt with or without attached devices (placed in `tools/test`):

```
    test_cal_param      crosscheck fw vs uqt known cal params   
    test_uqt_sanity     quick sanity check on uqt commands
    test_meas_sanity    quick sanity check on meas commands
    test_uqt            sanity check on uqt command Vs communication with 1 DUT
```


#### Handling Obfuscation
```
Obsolete!!! However, obfuscation has not been moved to uwb-qorvo-tools repo. So, if you need it, you know where to search
```
A (crude) obfuscation is available using the `py_obfuscate` script from umeas.  

This script should be run on one of the radar demo folder after release branch creation:
```
	py_obfuscate /x/uwb-meas/lib-ext/uwb-qorvo-tools/scripts/radar_tracking_demo
```


## Developer Guide

### Contributing Process

Thanks to follow below process:
- open a related ticket with some explanation.
- draw a branch with whatever name format BUT containing the full jira number. 
  (some examples: UWBMQA-1582/UQT_Add_Argument_to_run_fira_owr`, `feat/UWBMQA-3839_add_owr_aoa_support`, `hb.UWBMQA-4382`, ...)
- when your development is finished, squash your commits if needed and verify that your patches are full-filling below policy.
- Raise an MR and submit to axel.marty@qorvo.com, `Loic.Pasquier@qorvo.com`, or `cedric.ducottet@qorvo.com` 


### Patch Policy

- keep 1 'logical' change per commit.  
  **rational**: easier to understand and revert or cherry-pick
- Disable any auto-formatting script from your IDE
  (changing all line ending, 'fixing' all spaces in the source file, ...)  
  If you want to refactor for a 'better' format, do it in a dedicated patch  
  **rational**: real important changes are lost within your overall 'cosmetics' changes.
- Don't change `.md` files end of line spaces  
 **rational**: 2 spaces define a line break in markdown... 
- don't commit the sub-modules link information.  
  **rational**: easier to cherry-pick between branches.
- all commits are expected to be of below format  
  **rational**: getting maximum information from the tittle/one-liner.

```
<system>: <title> (<Jira tiket number>)`

with:
  <Jira tiket number> : when available
  <title>: starting with imperative action verb (fix, modify, add, ...) no final point
  <system>: one of below
    doc:             should have no impact on code
    refactoring:     id
    <script name>:   only this script change
    structure:       overall mod. Warning
    lib:             sub-module update.
                     Warning hidden possible issues:
                     look at git auto commit message (diff)
    purple:          specific to a customization
    radar:           id
    se_xxx:          impact Secure Element scripts
    cal_param:       cal param change.
    ...
    feel free to be imaginative if no system names
    are matching your needs

Examples:
   uqt_ls__brown: add this file to track customer delivery (UWBMQA-1294)
   doc: update history
   struct: handle UQT version request through scripts
   lib: update to latest uwb-uci
   decode_uci: add this script to decode external byte streams (UWBMQM35-4946)
   run_ccc_fira_twr: add a -d options to allow testing with reduced timing (UWBMQM35-4331)
```



### Compiling a Small Tool

Small tools such as `qm35-poll` have not other dependencies than libc.
Such binaries may be statically (and quickly) compiled locally using below commands
expected to be run on an UBUNTU machine:

```
    # 32b compile (uname -m == armv7l):
        sudo apt install gcc make gcc-arm-linux-gnueabi binutils-arm-linux-gnueabi
        arm-linux-gnueabi-gcc <my-file.c> -o <my-file> -static
    # 64b compile (uname -m == aarch64):
        sudo apt install gcc make gcc-aarch64-linux-gnu binutils-aarch64-linux-gnu
        aarch64-linux-gnu-gcc  <my-file.c> -o <my-file> -static
```

### Understanding the structure
 ```
todo
```


### Developer FAQ & Howtos
 ```
todo
```


#### Why All these Variant/Customization stuff ?
 ```
todo
```


#### How is Customization Handled in uwb-uci ?
 ```
todo
```

#### How is Customization Handled in uqt ?
 ```
todo
```


#### Where Should I Put my Code ?
 ```
todo
```


#### How to Add a New Customization in uwb-uci ?

Below import process is undergone in uwb-uci for the customization `<my customization>`:
 ```
# importing of fira.py
   ... and core.py, transport.py
   ... and all the fira_xx.py scripts
# importing of qorvo.py
	... and all qorvo_xx.py
# importing of custom.py
   ... xor custom_<my customization>.py if available
# importing of all addin_xx.py
   ... xor addin_xx__<my customization>.py if available
```

Create `<my_customization>.yaml` file in `tools/release/filters` and specify explicitly which files should be present in your customization and which should be deleted. See for more info in [release README](tools/release/README.md)

don't forget to file the `__all__` list to have your new objects loaded in the `uci` name-space

#### How to Add a New Customer Script (in uqt) ?
 ```
# Spend time to choose a proper name to your script !!!
  short and expressive; starting with a verb:
  run_something, test_other_thing, start_something_else ...
# Create your script in uwb-qorvo-tools/bin
  You may use run_fira_twr as a 'template' 
  (it contains the up-to date structure)
# Add your script to the cmd test list, to have it part of 
  the non-regression process:
  add an entry to umeas/bin/uqt_cmd_test_list
```

#### How to Add a New Test Script (in umeas) ?
 ```
# Spend time to choose a proper name to your script !!!
  short and expressive; starting with a verb:
  run_something, test_other_thing, start_something_else ...
# Create your script in uwb-meas/bin
# if your script is controlling only 1 type of DUT/variant, 
  please import the uci namespace:
  this allow your script to operate (usually) over all variants.
# if your script is controlling several DUT type, 
  you should import the specific uci_<colored>  namespace.  
  You may use the umeas 'test_com_interop' script as a 'template' 
  (it contains the up-to date structure)
# Add your script to meas_ls
```

#### How to Add/Modify/Remove a New UCI command ?
 ```
todo
```


#### How to Add/Modify/Remove a Calibration Parameter ?
 ```
todo
```

#### How to Add/Modify/Remove a Session Parameter ?
 ```
todo
```

#### How to Add/Modify/Remove a Configuration Parameter ?
 ```
todo
```

#### How to verify You didn't Break Other Scripts ?

In the umeas repo, several test scripts are available to test each uqt script separately and evaluate uqt synchronisaiton Vs firmware (cal params, session params).   
cf. `meas_ls` under the `Developper/Maintener commands`.  
Feel free to run them to ensure everything is ok.    

```
# Without device, run a quick test (low test coverage):
  test_uqt_sanity -fv  
# With 1 connected device, verify the 'gold' scripts:
  test_uqt -fv -p /dev/ttyUSB1
# With 1 connected device, verify the 'purple' scripts:
  test_uqt -fv -c purple -p /dev/ttyUSB1  
# With 1 connected device, verify the calibration parameter list:
  test_uqt_cal_params -v -p /dev/ttyUSB1  
# With 1 connected device, verify the session parameter list:
  test_uqt_session_params -v -p /dev/ttyUSB1  
```

## History

[main branch history](history.md)


