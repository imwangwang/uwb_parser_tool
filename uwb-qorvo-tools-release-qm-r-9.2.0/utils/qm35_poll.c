/*
 * Copyright (c) 2022 Qorvo US, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at: http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */

#define _GNU_SOURCE 1

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


#define UCI_DEV "/dev/uci"
// in hexascii: header (4*2) + max payload (0xff*2) + "\r\n"
#define BUFFER_SIZE (4*2 + 0xff*2 + 2)
#define MAX_EVENTS 5
#define NB_RETRIES 3
#define DELAY_BETWEEN_RETRIES_S 1

// Send a binary or str buffer to uci as binary data
int to_uci(char *buffer, size_t size, int uci_fd, int from_bin)
{
	int n, idx, retry, write_size;
	char *internal_buffer = 0;
	int rts = 0;

	// Maybe convert from str to bin:
	if (from_bin) {
		internal_buffer = buffer;
		write_size = size;
	} else {
		if ((buffer[size-1] == '\n') || (buffer[size-1] == '\r')) size -= 1;
		if ((buffer[size-1] == '\n') || (buffer[size-1] == '\r')) size -= 1;
		if ((size % 2) != 0) {
			printf("to_uci() fails: request to write an odd number of nibbles (%zd).\n", size);
			rts = EIO;
			goto error;
		}
		write_size = size / 2;
		internal_buffer = malloc(write_size);
		if (!internal_buffer) {
			rts = errno;
			printf("malloc() fails: %s. Quitting\n", strerror(rts));
			goto error;
		}
		for (int i = 0; i < write_size; i++) {
			rts = sscanf(buffer + 2 * i, "%02hhx", &internal_buffer[i]);
			if (rts != 1) {
				rts = errno;
				printf("sscanf() fails: %s. Quitting\n", strerror(rts));
				goto error;
			}
		}
	}

	// output to uci_fd:
	idx = 0;
	retry = NB_RETRIES;
	while (retry != 0) {
		n = TEMP_FAILURE_RETRY(write(uci_fd, internal_buffer+idx, write_size));
		if (n < 0) {
			rts = errno;
			retry--;
		} else if (n != write_size) {
			idx = idx+n;
			write_size = write_size-idx;
			rts = 0;
			retry = NB_RETRIES;
		} else {
			retry = 0;
			rts = 0;
		}
		if (retry != 0) sleep(DELAY_BETWEEN_RETRIES_S);
	}
	if (rts != 0) printf(
			"write(uci_fd) fails. Bytes written: %u/%zd\n. Error: %s(%i)\n",
			idx, size, strerror(rts), rts);
error:
	if ((!from_bin) && (internal_buffer)) free(internal_buffer);
	return rts;
}

// Send a binary buffer to stdout as binary or string data
int to_stdout(char *buffer, size_t size, int as_bin)
{
	if (as_bin) {
		int rts = 0;
		int n = TEMP_FAILURE_RETRY(write(STDOUT_FILENO, buffer, size));
		if (n < 0) {
			rts = errno;
			printf("write(STDOUT_FILENO) fails: %s\n", strerror(rts));
		} else if (n != size) {
			printf("write(STDOUT_FILENO) fails. Bytes written: %u/%zd\n", n, size);
			rts = EIO;
		}
		return rts;
	} else {
		for (int i = 0; i < size; i++) printf("%02hhx", buffer[i]);
		printf("\n");
		return 0;
	}
}


int main(int argc, char *argv[])
{
	int uci_fd = 0;
	int epoll_fd = 0;
	int nfds;
	int is_bin_com = 0;
	struct epoll_event ev, events[MAX_EVENTS];
	int rts = 0;

	if (argc >= 2 && strcmp(argv[1], "-b") == 0) {
		// Binary Communication used.
		is_bin_com = 1;
	}

	uci_fd = open(UCI_DEV, O_RDWR);
	if (uci_fd < 0){
		rts = errno;
		printf("open(" UCI_DEV ") fails: %s. Quitting\n", strerror(rts));
		return rts;
	}

	epoll_fd = epoll_create1(0);
	if (epoll_fd < 0){
		rts = errno;
		printf("epoll_create1() fails: %s. Quitting\n", strerror(rts));
		goto error;
	}

	ev.events = EPOLLIN;
	ev.data.fd = STDIN_FILENO;
	if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, 0, &ev) < 0) {
		rts = errno;
		printf("epoll_ctl(STDIN_FILENO) fails: %s. Quitting\n", strerror(rts));
		goto error;
	}

	ev.events = EPOLLIN;
	ev.data.fd = uci_fd;
	if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, uci_fd, &ev) < 0) {
		rts = errno;
		printf("epoll_ctl(uci_fd) fails: %s. Quitting\n", strerror(rts));
		goto error;
	}

	while (1) {

		nfds = TEMP_FAILURE_RETRY(epoll_wait(epoll_fd, events, MAX_EVENTS, -1));
		if (nfds < 0) {
			rts = errno;
			printf("epoll_wait() fails: %s\n", strerror(rts));

			goto error;
		}

		for (int i = 0; i < nfds; i++) {
			char buffer[BUFFER_SIZE];
			int in_fd = events[i].data.fd;
			ssize_t nread;

			nread = TEMP_FAILURE_RETRY(read(in_fd, buffer, BUFFER_SIZE));
			if (nread < 0) {
				rts = errno;
				printf("read(in_fd) fails: %s\n", strerror(rts));
				goto error;
			}

			if (in_fd == STDIN_FILENO) {
					rts = to_uci(buffer, nread, uci_fd, is_bin_com);
					if (rts != 0) goto error;
			} else {
					rts = to_stdout(buffer, nread, is_bin_com);
					if (rts != 0) goto error;
			}
		}
	}
error:
	if (uci_fd) close(uci_fd);
	return rts;
}
