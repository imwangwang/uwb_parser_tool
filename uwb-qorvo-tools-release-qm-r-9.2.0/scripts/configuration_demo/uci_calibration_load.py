#!/usr/bin/env python3
# Copyright (c) 2023 Qorvo US, Inc.

import argparse
import logging

from uci import Client
from load_calibration import load_calibration

parser = argparse.ArgumentParser(description='Update the provided Calibration'
                                             ' in the target through UCI.')
parser.add_argument('-f', '--calibration',
                    type=str,
                    help='Calibration JSON file to use. '
                         'Default: sip_calibration.json',
                    default='calib_files/QM35725_evb_revb/jolie_omni_jolie_aoa.json')
parser.add_argument('-p', '--port',
                    type=str,
                    help='Port to use (FT2232, ftdi://FT4222, /dev/ttyUSBX, COMX). '
                         'Default: ftdi://FT4222',
                    default='ftdi://FT4222')
parser.add_argument('-v', '--verbose', action='store_true',
                    help='use logging.DEBUG level',
                    default=False)


args = parser.parse_args()


if args.verbose:
    logging.getLogger().setLevel(logging.DEBUG)

client = Client(port=args.port)

load_calibration(client=client, calibration_filename=args.calibration)

client.close()
