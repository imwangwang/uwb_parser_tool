#! python

import argparse
import datetime
import json
import logging
import os
import queue
import sys
from time import sleep, time

import numpy as np

from radar_runner import RadarRunner
from radar import RadarConfigDecoder
from frame_log_writer import FrameLogWriter

sys.stdin.reconfigure(encoding='utf-8')
sys.stdout.reconfigure(encoding='utf-8')

DEFAULT_CALIBRATION_FILE = os.path.dirname(__file__) + "/radar_calibration.json"
# Using custom types from uci.utils to force correct
# type and parsing in tvs_to_bytes in core.py
DEFAULT_SETTINGS =  '{'\
                    '"RadarConfiguration":'\
                        '{"ChannelNumber": 9,'\
                        '"RframeConfig": 0,'\
                        '"PreambleCodeIndex": 9,'\
                        '"PreambleDuration": 5,'\
                        '"SessionPriority": 50,'\
                        '"TimingParams": {'\
                        '   "burst_period_ms": 3,'\
                        '   "sweep_period_rstu": 12000,'\
                        '   "sweeps_per_burst": 1'\
                        '},'\
                        '"SamplesPerSweep": {"__type__": "Uint8", "value": 64},'\
                        '"SweepOffset": {"__type__": "Int16", "value": -10},'\
                        '"BitsPerSample": 1,'\
                        '"NumberOfBursts": {"__type__": "Uint16", "value": 0},'\
                        '"RadarDataType": {"__type__": "Uint8", "value": 0},'\
                        '"AntennaSetId": 2,'\
                        '"TxProfileIdx": 0},'\
                    '"DemoConfiguration":'\
                        '{"run_time": 30}'\
                    '}'

SEC_TO_MS = 1000

def main():
    parser = argparse.ArgumentParser(description="""Capture, save and\
    post process CIR data.""")
    parser.add_argument("-p", "--port",
                        help="Path to hsspi interface. Default: FT4222",
                        default=os.getenv('UQT_PORT', "ftdi://FT4222"))
    parser.add_argument("-o", "--output",
                        help="Path to saved data",
                        default="./log")
    parser.add_argument("-s", "--settings",
                        help='Radar settings JSON object file',
                        default=None)
    parser.add_argument("-c", "--calibration",
                        help='Device calibration JSON object file',
                        default=DEFAULT_CALIBRATION_FILE)
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='use logging.DEBUG level',
                        default=False)

    args = parser.parse_args()

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    if not os.path.exists(args.output):
        os.makedirs(args.output)
    log_file = (f"{args.output}/"
                f"{datetime.datetime.now().strftime('%y-%m-%d-%Hh%Mm%Ss')}"
                f"_accumulator_log.json")
    report_file = (f"{args.output}/"
                f"{datetime.datetime.now().strftime('%y-%m-%d-%Hh%Mm%Ss')}"
                f"_report.txt")

    try:
        if args.settings:
            with open(args.settings, 'r') as f:
                settings = json.load(f, object_hook=RadarConfigDecoder)
        else:
            settings = json.loads(DEFAULT_SETTINGS, object_hook=RadarConfigDecoder)

        print(settings)

    except Exception as e:
        print(f"Invalid Radar Settings.\n{e}")
        exit(-1)

    assert settings
    radar_config = settings["RadarConfiguration"]
    demo_config = settings["DemoConfiguration"]
    exp_frames =  round(demo_config['run_time'] * SEC_TO_MS / radar_config['TimingParams']['burst_period_ms'])

    ntfs_queue =  queue.Queue()

    try:
        radar_runner = RadarRunner(port=args.port, radar_config=radar_config, calibration=args.calibration, out_queue=ntfs_queue)
        log_runner = FrameLogWriter(fname=log_file, radar_config=radar_config, in_queue=ntfs_queue)
        demo_run_time = demo_config['run_time']
        print(f'Expecting running time: {demo_run_time}s. Frames expecting: {exp_frames}')

        radar_runner.start()
        log_runner.start()

        start = time()
        sleep(demo_run_time)
        end = time()

        radar_runner.stop()
        radar_runner.join()
        
        log_runner.stop()
        log_runner.join()
        
        print_stats(report_file, end-start, exp_frames, radar_runner.nb_frames, radar_runner.ok_frames, radar_runner.error_frames, radar_runner.rfri_list)

    except KeyboardInterrupt:
        print('Interrupted')
        if radar_runner:
            radar_runner.stop()
            radar_runner.join()
        if log_runner:
            log_runner.stop()
            log_runner.join()
        print_stats(report_file, exp_frames, radar_runner.nb_frames, radar_runner.ok_frames, radar_runner.error_frames, radar_runner.rfri_list)

        try:
            sys.exit(0)
        except Exception:
            os._exit(0)

def print_stats(fname, demo_duration, exp_frames, rec_frames, ok_frames, error_frames, rfri_list):
    print(f'Calculating statistics...')

    rfri = np.array(rfri_list[1:])
    rfri_mean = np.mean(rfri)
    rfri_std = np.std(rfri)
    rfri_rstd = rfri_std / rfri_mean
    line_1 = f'Running time: {demo_duration:.4f}s.'
    line_2 = f'Frames expected: {exp_frames}. Frames collected: {rec_frames}. OK frames number: {ok_frames}. Error frames number: {error_frames}'
    line_3 = f'Mean RFRI: {rfri_mean:.4f}ms. RFRI std: {(rfri_std):.4f}ms. RFRI relative std: {(rfri_rstd * 100 ):.2f}%'

    print(line_1)
    print(line_2)
    print(line_3)

    with open(fname, "w+") as file:
        file.write(f'{line_1}\n')
        file.write(f'{line_2}\n')
        file.write(f'{line_3}\n')

if __name__ == "__main__":
    main()
