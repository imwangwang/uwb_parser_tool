#
# Created on Mon Apr 03 2023
#
# Copyright (c) 2022 Qorvo, Inc
# All rights reserved.
# NOTICE: All information contained herein is, and remains the property
# of Qorvo, Inc. and its suppliers, if any. The intellectual and technical
# concepts herein are proprietary to Qorvo, Inc. and its suppliers, and
# may be covered by patents, patent applications, and are protected by
# trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly forbidden unless prior written
# permission is obtained from Qorvo, Inc.
#
import threading
import queue
import json
import os
from radar import RadarDataEncoder

class FrameLogWriter(threading.Thread):
    def __init__(self, fname, radar_config, in_queue):
        super().__init__()
        self.in_queue = in_queue
        self.stopped = False
        
        frames = {'Configuration': {
            'ChannelNumber': radar_config["ChannelNumber"],
            'RframeConfig': radar_config["RframeConfig"],
            'PreambleCodeIndex': radar_config["PreambleCodeIndex"],
            'PreambleDuration': radar_config["PreambleDuration"],
            'burst_period_ms': radar_config["TimingParams"]["burst_period_ms"],
            'sweep_period_rstu': radar_config["TimingParams"]["sweep_period_rstu"],
            'sweeps_per_burst': radar_config["TimingParams"]["sweeps_per_burst"],
            'SamplesPerSweep': radar_config["SamplesPerSweep"],
            'SweepOffset': radar_config["SweepOffset"],
            'BitsPerSample': radar_config["BitsPerSample"],
            'NumberOfBursts': radar_config["NumberOfBursts"],
            'RadarDataType': radar_config["RadarDataType"],
            'AntennaSetId': radar_config["AntennaSetId"],
            'TxProfileIdx': radar_config["TxProfileIdx"]
        }
        }

        self.file = open(fname, "w+")
        self.file.write(json.dumps(frames, indent=4, cls=RadarDataEncoder))

        # Move the cursor to the end of the file
        self.file.seek(0, os.SEEK_END)
        # Delete closing bracket and new line before it 
        pos = self.file.tell() - 2
        self.file.seek(pos, os.SEEK_SET)
        self.file.truncate()
        # Set the cursor to the last character after the deletion
        self.file.seek(0, os.SEEK_END)
        # Write ,\n"Frames": [
        self.file.write(',\n    "Frames": [\n')
    
    def run(self):
        while True:
            try:
                item = self.in_queue.get(timeout=1)
                json_data = json.dumps(item, indent=4, cls=RadarDataEncoder)

                self.file.seek(0, os.SEEK_END)
                self.file.write(json_data)
                self.file.write(',\n')

            except queue.Empty:
                if not self.stopped:
                    pass
                else:
                    break

        # Delete last comma and new line
        self.file.seek(0, os.SEEK_END)
        pos = self.file.tell() - 2
        self.file.seek(pos, os.SEEK_SET)
        self.file.truncate()
        # Write end of JSON 
        self.file.write("]\n}")
        self.file.close()

    def stop(self):
        self.stopped = True
