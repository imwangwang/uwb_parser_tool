import sys
import queue
import threading
from enum import Enum, auto

from radar import RadarClient
from uci import UciComError, Status
from load_calibration import load_calibration

RSTU_TO_MS=1200
DTU_TO_RSTU=13
TIMESTAMP_RSTU_MAX = int(0xFFFFFFFF / DTU_TO_RSTU)

class StopState(Enum):
    NONE = auto()
    STOP_REQUESTED = auto()
    STOPPING = auto()
    STOPPED = auto()


class RadarRunner(threading.Thread):
    def __init__(self, port, radar_config, calibration, out_queue, **kwargs):
        super().__init__()
        self._radar = RadarClient(port=port)
        self.out_queue = out_queue
        self.stop_state = StopState.NONE
        self.burst_period = radar_config['TimingParams']['burst_period_ms']
        self.last_rfri = 0
        self.rfri_list = []
        self._last_timestamp = 0

        if calibration:
            load_calibration(self._radar._client, calibration)
            
        try:
            ret, _ = self._radar.radar_enable()
            if ret != Status.Ok:
                self.__print_status_error(ret, "Unable to Enable Radar:")
                return
            ret = self._radar.radar_set_config(radar_config)
            if ret != Status.Ok:
                self.__print_status_error(ret, "Unable to Configure Radar:")
                return
        except UciComError as e:
            self.__print_protocol_error(e)
            sys.exit(-1)

    def run(self) -> None:
        try:
            self.nb_frames = 0
            self.ok_frames = 0
            self.error_frames = 0
            ret = self._radar.radar_start()
            if ret != Status.Ok:
                self.__print_status_error(ret, "Unable to Start Radar:")
                return
            print('Running...')
            while self.stop_state != StopState.STOPPED:
                try:
                    entry = self._radar.radar_get_data()
                    self.out_queue.put(entry)
                    if entry.status_code == 0:
                        self.ok_frames += 1
                        timestamp = entry.sweep_data[0].timestamp
                    else:
                        self.error_frames += 1
                        timestamp = (self._last_timestamp + self.burst_period) % TIMESTAMP_RSTU_MAX
                    self.nb_frames += 1

                    if timestamp > self._last_timestamp:
                        timestamp_diff = timestamp - self._last_timestamp
                    else:
                        timestamp_diff = (TIMESTAMP_RSTU_MAX - self._last_timestamp) + timestamp

                    self.last_rfri = timestamp_diff / RSTU_TO_MS
                    self._last_timestamp = timestamp
                    self.rfri_list.append(self.last_rfri)
                    if self.nb_frames % 200 == 0:
                        print(f'\rNTFs received: {self.nb_frames}. Last RFRI={self.last_rfri:.2f}ms', end=' ')

                except queue.Empty:
                    print("No CIR in queue \n")
                    if self.stop_state == StopState.STOPPING:
                        self.stop_state = StopState.STOPPED
                        
                if self.stop_state == StopState.STOP_REQUESTED:
                    self.__try_stop()
                    self.stop_state = StopState.STOPPING


        except UciComError as e:
            self.__print_protocol_error(e)
            sys.exit(-1)

        finally:
            try:
                print('Disabling...')
                ret = self._radar.radar_disable()
                print(str(ret))
            except UciComError as e:
                self.__print_protocol_error(e)
                sys.exit(-1)

    def __try_stop(self):
        try:
            print('\nStopping...')
            ret = self._radar.radar_stop()
            print(str(ret))
        except UciComError as e:
            self.__print_protocol_error(e)
            sys.exit(-1)


    def stop(self):
        self.stop_state = StopState.STOP_REQUESTED
    
    def __print_protocol_error(self, e: UciComError):
        print("\n*******************ERROR*********************")
        print("Command raised a Communication Error:")
        print(f"{e}")
        print("*********************************************")


    def __print_status_error(self, s: Status, error: str):  
        print("\n*******************ERROR*********************")
        print(error)
        print(f"{str(s)}")
        print("*********************************************")