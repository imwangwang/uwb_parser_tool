#Copyright (c) 2022 Qorvo, Inc
#All rights reserved.
#NOTICE: All information contained herein is, and remains the property
#of Qorvo, Inc. and its suppliers, if any. The intellectual and technical
#concepts herein are proprietary to Qorvo, Inc. and its suppliers, and
#may be covered by patents, patent applications, and are protected by
#trade secret and/or copyright law. Dissemination of this information
#or reproduction of this material is strictly forbidden unless prior written
#permission is obtained from Qorvo, Inc.

import threading
import queue
from time import time
from serial import Serial

from uci import UciComError, Status
from radar import RadarClient
from load_calibration import load_calibration

import sys
sys.stdin.reconfigure(encoding='utf-8')
sys.stdout.reconfigure(encoding='utf-8')


def print_protocol_error(e: UciComError):
    print("\n*******************ERROR*********************")
    print("Command raised a Communication Error:")
    print(f"{e}")
    print("*********************************************")


def print_status_error(s: Status, error: str):
    print("\n*******************ERROR*********************")
    print(error)
    print(f"{str(s)}")
    print("*********************************************")


class RadarRunner(threading.Thread):
    def __init__(self, port, radar_settings, calibration, out_queue, out_to_log_queue, hw_fc=True, reset=True, **kwargs):
        super().__init__()
        self._radar = RadarClient(port=port)

        self.out_queue = out_queue
        self.out_to_log_queue = out_to_log_queue

        self.stopped = False

        if calibration:
            load_calibration(self._radar._client, calibration)

        try:
            ret, _ = self._radar.radar_enable()
            if ret != Status.Ok:
                print_status_error(ret, "Unable to Enable Radar:")
                return
            ret = self._radar.radar_set_config(radar_settings)
            if ret != Status.Ok:
                print_status_error(ret, "Unable to Configure Radar:")
                return
        except UciComError as e:
            print_protocol_error(e)
            sys.exit(-1)

    def run(self) -> None:
        try:
            self.nb_frames = 0
            start = 0
            self.stopped = False

            ret = self._radar.radar_start()
            if ret != Status.Ok:
                print_status_error(ret, "Unable to Start Radar:")
                return
            print('Running...')
            start = time()
            while not self.stopped:
                try:
                    entry = self._radar.radar_get_data()
                except queue.Empty:
                    print("No CIR in queue \n")
                    continue
                if entry:
                    self.nb_frames += 1
                    # By default take data from first sweep
                    raw_data = {"re": [i.re for i in  entry.sweep_data[0].sample_data],
                                "im": [i.im for i in  entry.sweep_data[0].sample_data],
                                "z": [i.z for i in  entry.sweep_data[0].sample_data]
                               }
                    self.out_queue.put(raw_data)
                    if self.out_to_log_queue is not None:
                        self.out_to_log_queue.put(entry)
        except UciComError as e:
            print_protocol_error(e)
            sys.exit(-1)
        finally:
            stop = time()
            try:
                print('Stopping...')
                ret = self._radar.radar_stop()
                print(str(ret))
                ret = self._radar.radar_disable()
                print(str(ret))
            except UciComError as e:
                print_protocol_error(e)
                sys.exit(-1)
            finally:
                self._radar._client.close()
                self.stopped = True
                print('Stopped...')
                if start:
                    print(f'{self.nb_frames} frames in\
                    {stop - start}s ({self.nb_frames/(stop-start)} fps)')

    def stop(self):
        self.stopped = True
