#
# Created on Mon Apr 03 2023
#
# Copyright (c) 2022 Qorvo, Inc
# All rights reserved.
# NOTICE: All information contained herein is, and remains the property
# of Qorvo, Inc. and its suppliers, if any. The intellectual and technical
# concepts herein are proprietary to Qorvo, Inc. and its suppliers, and
# may be covered by patents, patent applications, and are protected by
# trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly forbidden unless prior written
# permission is obtained from Qorvo, Inc.
#
import threading
import queue
import json
import os
from radar import RadarDataEncoder


class FrameLogWriter(threading.Thread):
    def __init__(self, fname, radar_config, in_queue):
        super().__init__()
        self.in_queue = in_queue
        self.stopped = False

        frames = {'Configuration': {
            'ChannelNumber': radar_config["ChannelNumber"],
            'RframeConfig': radar_config["RframeConfig"],
            'PreambleCodeIndex': radar_config["PreambleCodeIndex"],
            'PreambleDuration': radar_config["PreambleDuration"],
            'burst_period_ms': radar_config["TimingParams"]["burst_period_ms"],
            'sweep_period_rstu': radar_config["TimingParams"]["sweep_period_rstu"],
            'sweeps_per_burst': radar_config["TimingParams"]["sweeps_per_burst"],
            'SamplesPerSweep': radar_config["SamplesPerSweep"],
            'SweepOffset': radar_config["SweepOffset"],
            'BitsPerSample': radar_config["BitsPerSample"],
            'NumberOfBursts': radar_config["NumberOfBursts"],
            'RadarDataType': radar_config["RadarDataType"],
            'AntennaSetId': radar_config["AntennaSetId"],
            'TxProfileIdx': radar_config["TxProfileIdx"]
        }
        }

        self.file = open(fname, "w+")
        self.file.write(json.dumps(frames, indent=4, cls=RadarDataEncoder))

        # Move the cursor to the end of the file
        self.file.seek(0, os.SEEK_END)

        # Get the position of the last newline character
        pos = self.file.tell() - 1
        while pos > 0 and self.file.read(1) != "\n":
            pos -= 1
            self.file.seek(pos, os.SEEK_SET)

        # Delete the last line
        self.file.truncate(pos)

        # Set the cursor to the last character after the deletion
        self.file.seek(0, os.SEEK_END)

        # wrtie ,\n"Frames": [
        self.file.write(',\n    "Frames": [\n')

    def run(self):
        try:
            while not self.stopped:
                try:
                    item = self.in_queue.get(timeout=1)

                    self.file.seek(0,2)
                    self.file.tell() -1
                    self.file.write(json.dumps(item, indent=4, cls=RadarDataEncoder))
                    self.file.write(',\n')

                except queue.Empty:
                    pass
        finally:
            # delete last comma
            self.file.seek(0, os.SEEK_END)
            pos = self.file.tell() - 1
            self.file.seek(pos, os.SEEK_SET)
            self.file.truncate()
            # write json end
            self.file.write("]\n}")
            # close file
            self.file.close()

    def stop(self):
        self.stopped = True
