# Copyright (c) 2022 Qorvo, Inc
# All rights reserved.
# NOTICE: All information contained herein is, and remains the property
# of Qorvo, Inc. and its suppliers, if any. The intellectual and technical
# concepts herein are proprietary to Qorvo, Inc. and its suppliers, and
# may be covered by patents, patent applications, and are protected by
# trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly forbidden unless prior written
# permission is obtained from Qorvo, Inc.

from PySide2.QtCore import QObject
from post_runner import PostRunner
from frame_log_writer import FrameLogWriter
import queue
import os
import json
import datetime

from radar_runner import RadarRunner
from radar import RadarConfigDecoder
from queue_multiplexor import QueueMultiplexor

algo_config = {
    "keep_only_closest_track": 1,
    "M": 1000,
    "mu_0": 0.1,
    "skip_square": 0,
    "skip_filter": 0,
    "rho_0": 0.05,
    "w_t": 2,
    "kst": 10,
    "ksp": 64,
    "minpts": 2,
    "eps": 3,
    "n_c_max": 4,
    "d_0": 0.6,
    "a_0": 1.5708,
    "m4t2d": 1,
    "n4t2d": 50,
    "m4t2c": 100,
    "n4t2c": 200,
    "m4c2d": 1,
    "n4c2d": 200,
    "sigma": 0.02,
    "delta": 0.02,
    "alpha": 0.01,
    "o_m": 5,
    "o_M": 5
}


class Model(QObject):
    def __init__(self, port, log_path):
        super().__init__()
        self.port = port
        self.log_path = log_path
        self.radar_active = False
        self.noise_ref_active = False
        self.noise_ref_on_going = False
        self.noise_ref_done = False

        self.initial_algo_settings = algo_config

        self.qm = QueueMultiplexor()
        self.post_out_queue = queue.Queue()

        if self.log_path is None:
            self.frames_log_in_queue = None
        else:   
            self.frames_log_in_queue =  queue.Queue()

        self.p_0 = None

        self.radar_runner = None
        self.post_runner = None
        self.log_runner = None

    def change_radar_state(self, value):
        self.radar_active = value
        with open(os.path.join(".", "radar_config.json"), 'r') as f:
            self.radar_settings = json.load(f, object_hook=RadarConfigDecoder)

        if self.radar_active:
            self.radar_runner = RadarRunner(
                port=self.port, radar_settings=self.radar_settings, calibration=os.path.join(".", "radar_calibration.json"), out_queue=self.qm, out_to_log_queue=self.frames_log_in_queue)

            if self.log_path is not None:
                # some error checking
                if not os.path.exists(self.log_path):
                    os.makedirs(self.log_path)

                log_name = (
                    "Accumulator_log_"f"{datetime.datetime.now().strftime('%y-%m-%d-%Hh%Mm%Ss')}.json")
                log_name_ = (
                    "Tracking_log_"f"{datetime.datetime.now().strftime('%y-%m-%d-%Hh%Mm%Ss')}.csv")
                frames_log_fname = os.path.join(self.log_path, log_name)
                frames_log_fname_ = os.path.join(self.log_path, log_name_)
                self.log_runner = FrameLogWriter(
                    frames_log_fname,  self.radar_settings, self.frames_log_in_queue)
                
                self.post_runner = PostRunner(
                    self.initial_algo_settings, self.post_out_queue, self.p_0, frames_log_fname_, self.radar_settings['TimingParams']['burst_period_ms']*1e-3)
            else:
                self.post_runner = PostRunner(
                    self.initial_algo_settings, self.post_out_queue, self.p_0)

                

            self.qm.add_queue(self.post_runner.in_queue)

            # no antenna switching for now
            self.post_runner.ant_switch = False

            self.radar_runner.start()
            self.post_runner.start()
            if self.log_runner:
                self.log_runner.start()

        if not self.radar_active:
            if self.radar_runner and not self.radar_runner.stopped:
                self.radar_runner.stop()
                self.radar_runner.join()

            if self.post_runner:
                self.post_runner.stop()
                self.post_runner.join()

            if self.log_runner:
                self.log_runner.stop()
                self.log_runner.join()
