#! python

#Copyright (c) 2022 Qorvo, Inc
#All rights reserved.
#NOTICE: All information contained herein is, and remains the property
#of Qorvo, Inc. and its suppliers, if any. The intellectual and technical
#concepts herein are proprietary to Qorvo, Inc. and its suppliers, and
#may be covered by patents, patent applications, and are protected by
#trade secret and/or copyright law. Dissemination of this information
#or reproduction of this material is strictly forbidden unless prior written
#permission is obtained from Qorvo, Inc.

import argparse
import sys
from os import getenv

from main_window import MainWindow
from model import Model
from PySide2.QtWidgets import QApplication

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="""Live tracking demo""")
    parser.add_argument("-p", "--port",
                        help="Path to port",
                        required=True,
                        default=getenv('UQT_PORT', "ftdi://FT4222"))
    parser.add_argument("-o", "--output", 
                        help="Path to saved data",
                        required=False
                        )

    args = parser.parse_args()
    app = QApplication(sys.argv)
    model = Model(port=args.port, log_path=args.output)
    view = MainWindow(model)
    view.show()
    sys.exit(app.exec_())
    