#Copyright (c) 2022 Qorvo, Inc
#All rights reserved.
#NOTICE: All information contained herein is, and remains the property
#of Qorvo, Inc. and its suppliers, if any. The intellectual and technical
#concepts herein are proprietary to Qorvo, Inc. and its suppliers, and
#may be covered by patents, patent applications, and are protected by
#trade secret and/or copyright law. Dissemination of this information
#or reproduction of this material is strictly forbidden unless prior written
#permission is obtained from Qorvo, Inc.

from PySide2.QtWidgets import QMainWindow
from PySide2.QtCore import QTimer
import queue
import pyqtgraph as pg
from matplotlib.pyplot import cm, Normalize
import os
import json
import numpy as np


def get_slowtime(nb_frames, burst_period):
    time_index = np.linspace(0, burst_period * nb_frames, num=nb_frames)
    return time_index

def get_range(frame_length, sampling_rate):
    speed_of_light = 3e+8
    time_index = np.linspace(0, (1/sampling_rate) *
                             frame_length, num=frame_length)
    range_index = time_index * speed_of_light / 2
    return range_index

class PlotRawWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.showing = True
        self.in_queue = queue.Queue()

        with open(os.path.join(".", "view_config.json"), 'r') as f:
            self.view_settings = json.load(f)

        self.setup_ui()
        self.setup_pyqtgraph()
        self.setup_buffers()
        self.setup_timer()

    def setup_ui(self):
        self.title = 'Raw CIRs'
        self.top = 100
        self.left = 100
        self.width = 1000
        self.height = 600
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

    def setup_pyqtgraph(self):
        win = pg.GraphicsLayoutWidget()
        win.setBackground('w')
        self.setCentralWidget(win)
        p_abs_raw_1d = win.addPlot(row=0, col=0)
        p_abs_raw_2d = win.addPlot(row=0, col=1)

        p_abs_raw_1d.setTitle('Abs(CIR)', color='k', size='15px')
        p_abs_raw_2d.setTitle('Abs(CIR)', color='k', size='15px')

        styles = {'color': 'grey', 'font-size': '15px'}
        p_abs_raw_1d.setLabel('bottom', 'distance [m]', **styles)
        p_abs_raw_2d.setLabel('bottom', 'distance [m]', **styles)
        p_abs_raw_1d.setLabel('left', 'magnitude', **styles)
        p_abs_raw_2d.setLabel('left', 'time [s]', **styles)

        self.p_abs_raw_2d_l_axis = p_abs_raw_2d.getAxis('left')

        pen = pg.mkPen(color='k')
        self.curve_abs_raw_1d = p_abs_raw_1d.plot(
            pen=pen)
        self.img_abs_raw = pg.ImageItem()
        p_abs_raw_2d.addItem(self.img_abs_raw)

        self.cm = cm.jet

    def setup_buffers(self):
        with open(os.path.join(".", "radar_config.json"), 'r') as f:
            self.burst_period = 1e-3 * json.load(f)['TimingParams']['burst_period_ms']
        self.buffer_len = int(self.view_settings['buffer_len'] / self.burst_period)
        self.range_idx = get_range(64, 1e9)
        self.buffer_abs_raw_2d = np.zeros((self.buffer_len, 64))
        self.slowtime_idx_10 = get_slowtime(self.buffer_len, self.burst_period)

        self.m = 0

    def setup_timer(self):
        self.timer = QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(self.view_settings['q_refresh'])

    def update(self):
        try:
            while self.in_queue.qsize():
                msg = self.in_queue.get(timeout=1)
                re = np.array(msg['re'])
                im = np.array(msg['im'])
                acc = re + (1j * im)
                z = np.abs(acc)
                self.buffer_abs_raw_2d[self.m, :] = z

                self.curve_abs_raw_1d.setData(self.range_idx, z)
                norm = Normalize(
                    self.buffer_abs_raw_2d.min(), self.buffer_abs_raw_2d.max())
                c_buffer_abs_raw_2d = self.cm(norm(self.buffer_abs_raw_2d.T))
                self.img_abs_raw.setImage(c_buffer_abs_raw_2d)

                self.m += 1
                if self.m >= self.buffer_len:
                    self.m = self.buffer_len-1
                    self.buffer_abs_raw_2d = np.concatenate(
                        (self.buffer_abs_raw_2d[1:, :], np.zeros((1, 64))), axis=0)

                    self.slowtime_idx_10[:-1] = self.slowtime_idx_10[1:]
                    self.slowtime_idx_10[-1] = self.slowtime_idx_10[-1] + self.burst_period

                custom_ticks = list(enumerate(self.slowtime_idx_10))[::100]
                self.p_abs_raw_2d_l_axis.setTicks([[(v[0], str(np.round(v[1], 0)))
                                                    for v in custom_ticks]])

        except queue.Empty:
            pass

    def closeEvent(self, event):
        self.showing = False
        