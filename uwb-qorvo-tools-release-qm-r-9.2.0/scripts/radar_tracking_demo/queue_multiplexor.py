#Copyright (c) 2022 Qorvo, Inc
#All rights reserved.
#NOTICE: All information contained herein is, and remains the property
#of Qorvo, Inc. and its suppliers, if any. The intellectual and technical
#concepts herein are proprietary to Qorvo, Inc. and its suppliers, and
#may be covered by patents, patent applications, and are protected by
#trade secret and/or copyright law. Dissemination of this information
#or reproduction of this material is strictly forbidden unless prior written
#permission is obtained from Qorvo, Inc.

class QueueMultiplexor():
    def __init__(self, queues=[]):
        self.queues = list(queues)

    def add_queue(self, queue):
        self.queues.append(queue)

    def put(self, item):
        for q in self.queues:
            q.put(item)
