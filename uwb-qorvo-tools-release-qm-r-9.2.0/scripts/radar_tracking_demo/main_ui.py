#Copyright (c) 2022 Qorvo, Inc
#All rights reserved.
#NOTICE: All information contained herein is, and remains the property
#of Qorvo, Inc. and its suppliers, if any. The intellectual and technical
#concepts herein are proprietary to Qorvo, Inc. and its suppliers, and
#may be covered by patents, patent applications, and are protected by
#trade secret and/or copyright law. Dissemination of this information
#or reproduction of this material is strictly forbidden unless prior written
#permission is obtained from Qorvo, Inc.

from PySide2 import QtCore, QtGui, QtWidgets
from pyqtgraph import GraphicsLayoutWidget

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(996, 722)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.plot_raw_button = QtWidgets.QPushButton(self.centralwidget)
        self.plot_raw_button.setObjectName("plot_raw_button")
        self.gridLayout.addWidget(self.plot_raw_button, 0, 1, 1, 1)
        self.activate_radar_button = QtWidgets.QPushButton(self.centralwidget)
        self.activate_radar_button.setCheckable(True)
        self.activate_radar_button.setObjectName("activate_radar_button")
        self.gridLayout.addWidget(self.activate_radar_button, 0, 0, 1, 1)
        self.graph_widget = GraphicsLayoutWidget(self.centralwidget)
        self.graph_widget.setAutoFillBackground(False)
        self.graph_widget.setObjectName("graph_widget")
        self.gridLayout.addWidget(self.graph_widget, 2, 0, 1, 4)
        self.display_d0 = QtWidgets.QLabel(self.centralwidget)
        self.display_d0.setText("")
        self.display_d0.setObjectName("display_d0")
        self.gridLayout.addWidget(self.display_d0, 0, 3, 1, 1, QtCore.Qt.AlignHCenter)
        self.calibrate_d0 = QtWidgets.QLineEdit(self.centralwidget)
        self.calibrate_d0.setObjectName("calibrate_d0")
        self.gridLayout.addWidget(self.calibrate_d0, 0, 2, 1, 1, QtCore.Qt.AlignHCenter)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 996, 23))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.plot_raw_button.setText(_translate("MainWindow", "Raw Signal"))
        self.activate_radar_button.setText(_translate("MainWindow", "Activate Radar"))
        