#Copyright (c) 2022 Qorvo, Inc
#All rights reserved.
#NOTICE: All information contained herein is, and remains the property
#of Qorvo, Inc. and its suppliers, if any. The intellectual and technical
#concepts herein are proprietary to Qorvo, Inc. and its suppliers, and
#may be covered by patents, patent applications, and are protected by
#trade secret and/or copyright law. Dissemination of this information
#or reproduction of this material is strictly forbidden unless prior written
#permission is obtained from Qorvo, Inc.

import threading
import queue
import numpy as np
import csv

from post_processing_functions import *


class PostRunner(threading.Thread):
    """
    This thread takes as input a frame from the in_queue and puts the result 
    in the out_queue, the result is presented in a dictionary as follows:
    (we only send info of confirmed tracks)
    {
        track1_id : (
            track1_distance, track1_speed, 
            track1_pdoa1, track1_pdoa2, 
            track1_aoa, (track1_x, track1_y)
        ),

        track2_id : (
            track2_distance, track2_speed, 
            track2_pdoa1, track2_pdoa2, 
            track2_aoa, (track2_x, track2_y)
        )        
    } 
    """

    def __init__(self, init_settings, post_out_queue, p_0, log_fname=None, burst_period=None):
        super().__init__()
        self.settings = init_settings
        self.out_queue = post_out_queue
        self.in_queue = queue.Queue()
        self.log_fname = log_fname
        if self.log_fname is not None:
            self.file = open(self.log_fname, "w+")
            self.writer = csv.writer(self.file)
            self.burst_period = burst_period

        # noise whitening 
        self.p_0 = p_0
        self.stopped = False
        self.ant_switch = False

        self.x1 = None
        self.old_x1 = None
        self.x2 = None
        self.old_x2 = None
        self.a = None
        self.memory_mti1 = np.zeros(64)
        self.memory_mti2 = np.zeros(64)
        self.memory_enh = np.zeros(64)
        
        # list of track objects
        self.track_list = []
        # slowtime index
        self.m = 1

    def run(self):
        while not self.stopped:
            try:
                msg = self.in_queue.get(timeout=1)
                re = np.array(msg['re'])
                im = np.array(msg['im'])

                acc = re + (1j * im)

                if self.ant_switch:
                    #self.post_process_with_ant_switch(acc)
                    # not availabale 
                    exit()
                else:
                    # pdoa is set always to be zero 
                    self.post_process_with_single_ant(acc)

                # single target tracking, we only keep the closet track
                if self.settings['keep_only_closest_track'] and len(self.track_list):
                    min_distance = np.min(
                        list(map(lambda t: t.distance, self.track_list)))
                    tracks_to_keep = list(
                        filter(lambda t: t.distance == min_distance, self.track_list))
                    track_to_keep = tracks_to_keep[0]
                    self.track_list = [track_to_keep]

                # send distance and pdoas of each confirmed track
                confirmed_tracks = list(
                    filter(lambda t: t.status == 'confirmed', self.track_list))
                message_tracks = {}
                for t in confirmed_tracks:
                    message_tracks[id(t)] = (
                        t.distance,
                        t.speed,
                        #np.rad2deg(np.mod(t.pdoa1+np.pi, 2*np.pi) - np.pi),
                        #np.rad2deg(np.mod(t.pdoa2+np.pi, 2*np.pi) - np.pi),
                        #np.rad2deg(pdoas2aoa(t.pdoa1, t.pdoa2)),
                        #pdoas2position(t.pdoa1, t.pdoa2, t.distance)
                    )
                    if self.log_fname is not None:
                        self.writer.writerow([self.m*self.burst_period, t.distance])

                # send tracks with current enhanced mti and cfar output
                #message = (message_tracks, self.p, self.b)
                message = (message_tracks, self.p)
                self.out_queue.put(message)

                self.m += 1

            except queue.Empty:
                pass

    def stop(self):
        if self.log_fname is not None:
            self.file.close()
        self.stopped = True

    def post_process_with_single_ant(self, acc):
        # remove clutter
        x, self.memory_mti1 = clutter_removal_mti(
            acc, self.memory_mti1, self.m, mu_0=self.settings['mu_0'])

        # enhance signal
        self.p, self.memory_enh = enhancement(
            x, self.memory_enh, rho_0=self.settings['rho_0'], skip_square=self.settings['skip_square'], skip_filter=self.settings['skip_filter'])

        # noise whitening
        q = noise_whitening(self.p, self.p_0)

        # cfar
        self.b = detection(
            q, w_t=self.settings['w_t'], kst=self.settings['kst'], ksp=self.settings['ksp'])

        # clustering
        c = clustering(
            self.b, minpts=self.settings['minpts'], eps=self.settings['eps'], n_c_max=self.settings['n_c_max'])

        # distance estimation
        d = distance_estimation(c, d_0=self.settings['d_0'])

        # pdoa is zero
        a = np.zeros((2, d.shape[0]))

        # if there is no living track and there is a cluster
        if len(self.track_list) == 0 and np.count_nonzero(~np.isnan(d)) > 0:
            d_l = np.nanmin(d)
            a1_l = 0
            a2_l = 0
            new_track = track_initialization(d_l, a1_l, a2_l)
            self.track_list.append(new_track)

        # if there was at least one living track
        if len(self.track_list):
            assign_output = observation_to_track_association(
                self.track_list, d, a)

        for t in self.track_list:

            # filter tracks that were not assigned
            if assign_output[id(t)] is None:
                t.filter_not_assigned()
                t.update_assigned_history(False)

            # filter tracks that were assigned
            else:

                out = assign_output[id(t)]
                d_l = out[0]
                a1_l = out[1]
                a2_l = out[2]
                t.filter_assigned(
                    d_l, a1_l, a2_l, sigma=self.settings['sigma'], delta=self.settings['delta'], alpha=self.settings['alpha'])
                t.update_assigned_history(True)

            # gate
            t.update_gate(o_m=self.settings['o_m'], o_M=self.settings['o_M'])

            # track maintenance
            keep = t.maintain_track(m4t2d=self.settings['m4t2d'], n4t2d=self.settings['n4t2d'], m4t2c=self.settings['m4t2c'],
                                    n4t2c=self.settings['n4t2c'], m4c2d=self.settings['m4c2d'], n4c2d=self.settings['n4c2d'])
            if not keep:
                self.track_list.remove(t)
