#Copyright (c) 2022 Qorvo, Inc
#All rights reserved.
#NOTICE: All information contained herein is, and remains the property
#of Qorvo, Inc. and its suppliers, if any. The intellectual and technical
#concepts herein are proprietary to Qorvo, Inc. and its suppliers, and
#may be covered by patents, patent applications, and are protected by
#trade secret and/or copyright law. Dissemination of this information
#or reproduction of this material is strictly forbidden unless prior written
#permission is obtained from Qorvo, Inc.

from PySide2.QtWidgets import QMainWindow
from PySide2.QtCore import QTimer, Qt
from radar import RadarConfigDecoder
from main_ui import Ui_MainWindow
from plot_raw_window import PlotRawWindow
import numpy as np
from matplotlib.pyplot import cm, Normalize
import pyqtgraph as pg
import os 
import json
import queue
from colorhash import ColorHash


def get_slowtime(nb_frames, burst_period):
    time_index = np.linspace(0, burst_period * nb_frames, num=nb_frames)
    return time_index

def get_range(frame_length, sampling_rate):
    speed_of_light = 3e+8
    time_index = np.linspace(0, (1/sampling_rate) *
                             frame_length, num=frame_length)
    range_index = time_index * speed_of_light / 2
    return range_index

class MainWindow(QMainWindow):
    def __init__(self, model):
        super().__init__()
        self._model = model
        self.in_queue = self._model.post_out_queue
        self.plot_raw_win = None
        self.config_param_win = None

        with open(os.path.join(".", "view_config.json"), 'r') as f:
            self.view_settings = json.load(f)

        self._ui = Ui_MainWindow()
        self._ui.setupUi(self)
        current_value = self._model.initial_algo_settings['d_0']
        self._ui.display_d0.setText(f'Current distance offset : {current_value}m')
        self.setup_buffers()
        self.setup_pyqtgraph()
        self.connect_slots()
        self.setup_timer()

    def connect_slots(self):
        self._ui.activate_radar_button.clicked.connect(
            self._model.change_radar_state)
        self._ui.plot_raw_button.clicked.connect(self.on_plot_raw)
        self._ui.calibrate_d0.textChanged.connect(self.on_calibrate_d0)

    def setup_pyqtgraph(self):
        self._ui.graph_widget.setBackground('w')

        # create plots
        p_enh_mti_2d = self._ui.graph_widget.addPlot(row=0, col=0)
        p_enh_mti_1d = self._ui.graph_widget.addPlot(row=1, col=0)
        self.p_dist = self._ui.graph_widget.addPlot(row=0, col=1)
        self.p_dist_l = self._ui.graph_widget.addPlot(row=1, col=1)

        # set grids
        self.p_dist.showGrid(x=True, y=True, alpha=0.7)
        self.p_dist_l.showGrid(x=True, y=True, alpha=0.7)

        # set titles
        p_enh_mti_2d.setTitle('Enhanced MTI', color='k', size='15px')
        p_enh_mti_1d.setTitle('Enhanced MTI', color='k', size='15px')
        self.p_dist.setTitle('Distance', color='k', size='15px')
        self.p_dist_l.setTitle('Distance', color='k', size='15px')

        # set axis labels
        styles = {'color': 'grey', 'font-size': '15px'}
        p_enh_mti_2d.setLabel('left', 'distance [m]', **styles)
        p_enh_mti_2d.setLabel('bottom', 'time [s]', **styles)
        p_enh_mti_1d.setLabel('left', 'magnitude [m]', **styles)
        p_enh_mti_1d.setLabel('bottom', 'distance [m]', **styles)
        self.p_dist.setLabel('left', 'distance [m]', **styles)
        self.p_dist.setLabel('bottom', 'time [s]', **styles)
        self.p_dist_l.setLabel('left', 'distance [m]', **styles)
        self.p_dist_l.setLabel('bottom', 'time [s]', **styles)

        # set axis limits
        self.p_dist.setYRange(-0.5, 10)
        self.p_dist_l.setYRange(-0.5, 10)

        # set custom ticks for p_enh_mti_2d to display time in s and ditance in m
        p_enh_mti_2d_l_axis = p_enh_mti_2d.getAxis('left')
        custom_ticks_want = list(map(lambda x: np.argmin(
            np.abs(self.range_idx - x)), list(range(10))))
        custom_ticks = list(enumerate(self.range_idx))
        p_enh_mti_2d_l_axis.setTicks([[(v[0], str(np.round(v[1], 2)))
                                       for v in custom_ticks if v[0] in custom_ticks_want]])
        self.p_enh_mti_2d_b_axis = p_enh_mti_2d.getAxis('bottom')

        # add status label in center of angle, distance and loc plots
        self.dist_label = pg.LabelItem('Status', size="20px")
        self.dist_label.setParentItem(self.p_dist)
        self.dist_label.anchor(itemPos=(0.5, 0.5), parentPos=(0.5, 0.5))
        self.dist_l_label = pg.LabelItem('Status', size="20px")
        self.dist_l_label.setParentItem(self.p_dist_l)
        self.dist_l_label.anchor(itemPos=(0.5, 0.5), parentPos=(0.5, 0.5))

        # set data placeholders for plot updates
        self.img_enh_mti = pg.ImageItem()
        p_enh_mti_2d.addItem(self.img_enh_mti)
        pen = pg.mkPen(color='k', width=2)
        self.curve_p_enh_mti_1d = p_enh_mti_1d.plot(pen=pen, skipFiniteCheck=True)
        self.curve_distance_dic = {}
        self.curve_distance_l_dic = {}


        # graveyard dor dead tracks
        self.graveyard = {}

        self.cm = cm.jet

        self.lr = pg.LinearRegionItem(values=(0, self.view_settings['buffer_len']))
        self.p_dist_l.addItem(self.lr)

        def updatePlot():
            self.p_dist.setXRange(*self.lr.getRegion(), padding=0)

        def updateRegion():
            self.lr.setRegion(self.p_dist.getViewBox().viewRange()[0])
        self.lr.sigRegionChanged.connect(updatePlot)
        self.p_dist.sigXRangeChanged.connect(updateRegion)
        updatePlot()

    def setup_buffers(self):
        # read burst period from radar_config.json
        with open(os.path.join(".", "radar_config.json"), 'r') as f:
            self.burst_period = 1e-3 * json.load(f, object_hook=RadarConfigDecoder)['TimingParams']['burst_period_ms']

        self.buffer_len = int(self.view_settings['buffer_len'] / self.burst_period)
        self.buffer_l_len = 10 * self.buffer_len
        self.slowtime_idx = get_slowtime(self.buffer_len, self.burst_period)
        self.slowtime_l_idx = get_slowtime(self.buffer_l_len, self.burst_period)
        self.range_idx = get_range(64, 1e+9)
        self.buffer_enh_mti_2d = np.zeros((self.buffer_len, 64))
        self.buffer_distance_dic = {}
        self.buffer_distance_l_dic = {}

        self.m = 0
        self.m_l = 0

    def setup_timer(self):
        self.timer = QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(self.view_settings['q_refresh'])

    def update(self):
        try:
            while self.in_queue.qsize():
                message = self.in_queue.get(timeout=1)
                message_tracks = message[0]
                enh_mti = np.sqrt(message[1])

                self.buffer_enh_mti_2d[self.m, :] = enh_mti
                self.curve_p_enh_mti_1d.setData(self.range_idx, enh_mti)

                # deal with lost tracks
                for track_id in list(self.buffer_distance_dic.keys()):
                    if track_id not in message_tracks:
                        if np.isnan(self.buffer_distance_dic[track_id]).all():
                            # if the track is no longuer visible on the screen 
                            # save memory by deleting its entry from the dics
                            self.buffer_distance_dic.pop(track_id)
                        else:
                            # if the track is still visible on the screen fill the
                            # plots with nan values
                            self.buffer_distance_dic[track_id][self.m] = np.nan
                            
                for track_id in list(self.buffer_distance_l_dic.keys()):
                    if track_id not in message_tracks:
                        if np.isnan(self.buffer_distance_l_dic[track_id]).all():
                            # if the track is no longuer visible on the screen 
                            # save memory by deleting its entry from the dics
                            self.buffer_distance_l_dic.pop(track_id)
                        else:
                            # if the track is still visible on the screen fill the
                            # plots with nan values
                            self.buffer_distance_l_dic[track_id][self.m_l] = np.nan

                for track_id in message_tracks.keys():
                    
                    distance = message_tracks[track_id][0]

                    if track_id in self.buffer_distance_dic:
                        # update existing track
                        self.buffer_distance_dic[track_id][self.m] = distance
                    else:
                        # create new buffers for new track
                        self.buffer_distance_dic[track_id] = np.zeros(self.buffer_len)
                        self.buffer_distance_dic[track_id][:] = np.nan
                        self.buffer_distance_dic[track_id][self.m] = distance

                        # create curves for new track
                        color = ColorHash(track_id).rgb
                        pen = pg.mkPen(color=color, style=Qt.DotLine, width=4)
                        self.curve_distance_dic[track_id] = self.p_dist.plot(pen=pen)

                    if track_id in self.buffer_distance_l_dic:
                        self.buffer_distance_l_dic[track_id][self.m_l] = distance
                    else:
                        self.buffer_distance_l_dic[track_id] = np.zeros(self.buffer_l_len)
                        self.buffer_distance_l_dic[track_id][:] = np.nan
                        self.buffer_distance_l_dic[track_id][self.m_l] = distance

                        color = ColorHash(track_id).rgb
                        pen = pg.mkPen(color=color, style=Qt.DotLine, width=4)
                        self.curve_distance_l_dic[track_id] = self.p_dist_l.plot(pen=pen)                        


                for track_id in self.buffer_distance_dic.keys():
                    self.curve_distance_dic[track_id].setData(self.slowtime_idx, self.buffer_distance_dic[track_id])

                for track_id in self.buffer_distance_l_dic.keys():
                    self.curve_distance_l_dic[track_id].setData(self.slowtime_l_idx, self.buffer_distance_l_dic[track_id])

                norm = Normalize(self.buffer_enh_mti_2d.min(), self.buffer_enh_mti_2d.max())
                c_buffer_enh_mti_2d = self.cm(norm(self.buffer_enh_mti_2d))
                self.img_enh_mti.setImage(c_buffer_enh_mti_2d)

                if len(message_tracks) == 0:
                    self.dist_label.setText('Lost', color='r')
                    self.dist_l_label.setText('Lost', color='r')
                else:
                    dist_label = ''
                    
                    for track_id in message_tracks.keys():
                        color = ColorHash(track_id).hex

                        dist_text = "<font color=" + color + ">"
                        distance = message_tracks[track_id][0]
                        dist_text = dist_text + str(np.round(distance, 2)) + ' m'
                        dist_text = dist_text + "</font>"
                        dist_label = dist_label + ' ' + dist_text

                    self.dist_label.setText(dist_label)
                    self.dist_l_label.setText(dist_label)

                self.m += 1
                self.m_l += 1

                if self.m >= self.buffer_len:
                    self.m = self.buffer_len - 1
                    self.slowtime_idx[:-1] = self.slowtime_idx[1:]
                    self.slowtime_idx[-1] = self.slowtime_idx[-1] + self.burst_period

                    self.buffer_enh_mti_2d = np.concatenate(
                        (self.buffer_enh_mti_2d[1:, :], np.zeros((1, 64))), axis=0)

                    for buffer in self.buffer_distance_dic.values():
                        buffer[:-1] = buffer[1:]
                        buffer[-1] = np.nan

                    self.lr.setRegion((self.slowtime_idx[0], self.slowtime_idx[-2]))


                if self.m_l >= self.buffer_l_len:
                    self.m_l = self.buffer_l_len - 1
                    self.slowtime_l_idx[:-1] = self.slowtime_l_idx[1:]
                    self.slowtime_l_idx[-1] = self.slowtime_l_idx[-1] + self.burst_period  

                    for buffer in self.buffer_distance_l_dic.values():
                        buffer[:-1] = buffer[1:]
                        buffer[-1] = np.nan              

                custom_ticks = list(enumerate(self.slowtime_idx))[::int(self.view_settings['buffer_len']*4)]
                self.p_enh_mti_2d_b_axis.setTicks([[(v[0], str(np.round(v[1], 0)))
                                                    for v in custom_ticks]])

        except queue.Empty:
            pass
        

    def on_calibrate_d0(self):
        try:
            new_d0_value = float(self._ui.calibrate_d0.text())
            self._model.initial_algo_settings['d_0'] = new_d0_value
            self._ui.display_d0.setText(f'Current distance offset : {new_d0_value}m')
        except ValueError:
            pass

    def on_plot_raw(self):
        if (self.plot_raw_win == None or self.plot_raw_win.showing == False):
            self.plot_raw_win = PlotRawWindow()
            self._model.qm.add_queue(self.plot_raw_win.in_queue)
            self.plot_raw_win.show()

    def closeEvent(self, event):
        if self.plot_raw_win:
            self.plot_raw_win.close()

        if self.config_param_win:
            self.config_param_win.close()

        self._model.change_radar_state(False)
