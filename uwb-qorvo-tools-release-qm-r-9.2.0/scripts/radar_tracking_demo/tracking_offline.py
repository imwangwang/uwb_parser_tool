#! python

#Copyright (c) 2022 Qorvo, Inc
#All rights reserved.
#NOTICE: All information contained herein is, and remains the property
#of Qorvo, Inc. and its suppliers, if any. The intellectual and technical
#concepts herein are proprietary to Qorvo, Inc. and its suppliers, and
#may be covered by patents, patent applications, and are protected by
#trade secret and/or copyright law. Dissemination of this information
#or reproduction of this material is strictly forbidden unless prior written
#permission is obtained from Qorvo, Inc.

import argparse
import numpy as np
import json
import threading
import queue
from post_runner import PostRunner
from time import time, sleep
import matplotlib.pyplot as plt
from os.path import basename, splitext


algo_config = {
    "keep_only_closest_track": 1,
    "M": 1000,
    "mu_0": 0.1,
    "skip_square": 0,
    "skip_filter": 0,
    "rho_0": 0.05,
    "w_t": 2,
    "kst": 10,
    "ksp": 64,
    "minpts": 2,
    "eps": 3,
    "n_c_max": 4,
    "d_0": 0.6,
    "a_0": 1.5708,
    "m4t2d": 1,
    "n4t2d": 50,
    "m4t2c": 100,
    "n4t2c": 200,
    "m4c2d": 1,
    "n4c2d": 200,
    "sigma": 0.02,
    "delta": 0.02,
    "alpha": 0.01,
    "o_m": 5,
    "o_M": 5
}


def load_cir_from_json_stack(fname):
    f = open(fname, "r")
    data = json.load(f)

    cir_list = []
    for i in range(len(data)):
        list_re_imag_str = list(map(lambda x: x["samples"], 
                                    data[i]["sweepData"]))
        for re_imag_str in list_re_imag_str:
            cir_re = list(
                map(
                    lambda x: int(x.split(", ")[0].strip("(")),
                    re_imag_str,
                )
            )
            cir_im = list(
                map(
                    lambda x: int(x.split(", ")[1].strip(")")),
                    re_imag_str,
                )
            )
            cir_list.append(list(map(lambda x: x[0] + 1j * x[1], 
                                     zip(cir_re, cir_im))))
    cir = np.asarray(cir_list)
    return cir


class FakeRadar(threading.Thread):
    def __init__(self, cir, out_queue):
        super().__init__()
        self.data = cir
        self.nb_frames = cir.shape[0]
        self.m = 0
        self.out_queue = out_queue
        self.stopped = False

    def run(self):
        while not self.stopped:
            frame = {}
            frame['re'] = self.data[self.m, :].real
            frame['im'] = self.data[self.m, :].imag
            self.out_queue.put(frame)
            self.m += 1
            if self.m >= self.nb_frames:
                self.stopped = True

    def stop(self):
        self.stopped = True


def main():
    parser = argparse.ArgumentParser(
        description="""Tracking offline on a cir. A CSV file _tracking.csv """
                    """will be created in the current directory.""")
    parser.add_argument("-i",
                        "--input",
                        required=True,
                        help="Path to input JSON file containing the CIRs")
    parser.add_argument("-b",
                        "--bp", 
                        default=50,
                        type=int,
                        help="Burst period in ms (default=50ms)")

    args = parser.parse_args()
    burst_period = args.bp / 1000
    assert args.input

    try:
        cir = load_cir_from_json_stack(args.input)
    except FileNotFoundError:
        print("File Not Found")
        exit(-1)

    # load fakeradar thread
    radar_out_queue = queue.Queue()
    fake_radar = FakeRadar(cir, radar_out_queue)

    # init post runner
    post_out_queue = queue.Queue()
    p_0 = None
    post_runner = PostRunner(algo_config, post_out_queue, p_0)
    post_runner.in_queue = radar_out_queue
    post_runner.ant_switch = False

    # start post runner
    start_time = time()
    fake_radar.start()
    post_runner.start()

    # wait for post runner
    while post_runner.m < fake_radar.nb_frames:
        sleep(1)

    fake_radar.stop()
    fake_radar.join()
    post_runner.stop()
    post_runner.join()

    print("post_runner done in %s seconds" % (time() - start_time))
    print(post_runner.m - 1)
    print(fake_radar.nb_frames)

    # ghater results
    distances = {}
    m = 0
    try:
        while post_out_queue.qsize():
            message = post_out_queue.get()
            message_tracks = message[0]

            for track_id in message_tracks.keys():
                if track_id in distances.keys():
                    distances[track_id]['x'].append(m)
                    distances[track_id]['y'].append(
                        message_tracks[track_id][0])
                else:
                    distances[track_id] = {'x': [], 'y': []}

            for track_id in distances.keys():
                if track_id not in message_tracks.keys():
                    distances[track_id]['x'].append(m)
                    distances[track_id]['y'].append(np.nan)

            m = m + 1

    except queue.Empty:
        pass

    # plot results
    title = args.input + ' distance estimation'
    plt.figure()
    for track_id in distances.keys():
        plt.plot(np.array(distances[track_id]['x'])*burst_period, np.round(
            distances[track_id]['y'], 4), 'o', rasterized=True, markersize=1)
    plt.xlabel('time [s]')
    plt.ylabel('distance [m]')
    plt.title(title + ' tracking')
    plt.grid(True)

    # save output to csv file
    fout = splitext(basename(args.input))[0] + '_tracking.csv'

    for track_id in distances.keys():
        with open(fout, 'a') as f:
            ds = [d for d in distances[track_id]['y'] 
                  if not np.isnan(d)]
            ts = [t*burst_period 
                  for i, t in enumerate(distances[track_id]['x']) 
                  if not np.isnan(distances[track_id]['y'][i])]
            np.savetxt(f,
                       np.column_stack([ts, ds]),
                       delimiter=",",
                       fmt='%f')

    plt.show()


if __name__ == "__main__":
    main()
