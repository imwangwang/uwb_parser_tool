#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

import argparse, logging, os, sys

from uci import *
from utils import uqt_errno

#  Below hack sometimes required when operating on windows git-bash/msys2 
import sys
sys.stdin.reconfigure(encoding='utf-8')
sys.stdout.reconfigure(encoding='utf-8')

default_port=os.getenv('UQT_PORT', '/dev/ttyUSB0')

parser = argparse.ArgumentParser(description='Reset the device through UCI.')
parser.add_argument("--description", action="store_true", 
        help="show short description of the script", default=False)
parser.add_argument("-p", "--port", type=str, default=default_port, 
        help='communication port to use. (default: %(default)s)')
parser.add_argument('-v', '--verbose', action='store_true', default=False, 
        help='use logging.DEBUG level. (default: %(default)s)')

args = parser.parse_args()

if args.description:
    print(f"-> {os.path.basename(__file__):40s} {parser.description}")
    sys.exit(0)

if args.verbose:
    logging.getLogger().setLevel(logging.DEBUG)
log = logging.getLogger()

try:

    client = None
    client = Client(port=args.port)

    rts=client.reset()

except UciComError as e:
    rts=e.n
    log.critical(f"{e}")
        
if client: client.close()
sys.exit(uqt_errno(rts))
