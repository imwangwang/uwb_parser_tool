#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

import argparse, logging, time, os, sys, binascii

from uci import *
from utils import uqt_errno

default_port = os.getenv('UQT_PORT', '/dev/ttyUSB0')
actions = {'add': 0, 'delete': 1, 'add-with-short-skey': 2, 'add-with-long-skey': 3}

# Below hack sometimes required when operating on windows git-bash/msys2
sys.stdin.reconfigure(encoding="utf-8")
sys.stdout.reconfigure(encoding="utf-8")

epilog = f"""
Note:
    The SESSION_UPDATE_CONTROLLER_MULTICAST_LIST cmd is used.
    
Example of use:
    session_update_multicat_list  delete '[ 0x0a, 0, 0, 0x0b, 0, 0]'
    session_update_multicat_list  add    '[ 0x0a, 0xa0000000, 0, 0x0b, 45, 0]'
    session_update_multicat_list  add-with-short-skey '[ 0x0a, 40, "000102030405060708090A0b0c0d0e0f"]'
    session_update_multicat_list  add-with-short-skey '[ 0x0a, 40, "00.01.02.03.04.05.06.07.08.09.0A.0b.0c.0d.0e.0f"]'
"""

parser = argparse.ArgumentParser(
    description="Update the multicast list of controlee.",
    formatter_class=argparse.RawTextHelpFormatter, epilog=epilog)
parser.add_argument("--description", action="store_true", 
                    help="show short description of the script")
parser.add_argument("-p", "--port", type=str, default=default_port,
                    help='communication port to use. (default: %(default)s)')
parser.add_argument('-v', '--verbose', action='store_true', default=False,
                    help='use logging.DEBUG level. (default: %(default)s)')
parser.add_argument('-s', '--session', type=str, default='42',
                    help='set the session handle to use. (default: %(default)s)')
parser.add_argument('-t', '--time', type=int, default=0,
                    help='sleep time (s) before closing the UCI traffic channel\n'
                         'and exiting the script. -1: up to key pressed. (default: %(default)s)')
parser.add_argument('action', choices=actions, default='add', nargs='?',
                    help='set the action to execute on the list. (default: %(default)s)')
parser.add_argument('controlees', type=str, default='[ 0x0a, 0, 0 ]', nargs='?',
                    help='List of controlees with below format:\n'
                         '[MacId_1, ssID_1, ssKey_1 ..., MacId_n, ssID_n, ssKey_n]\n'
                         'with:\n'
                         '    Mac_Id_i : short address (Uint16)  of the newly added/removed Controlee\n'
                         '    ssID_i   : Controlee specific sub-session ID (Uint32)\n'
                         '        may be set to 0 if action is "delete"\n'
                         '    ssKey_n  : 0 or a 16 (or 32) bytes sub-session Key.\n'
                         '        May be expressed as a (space, dot or colon) delimited list of bytes as below:\n'
                         '        "000102030405060708090A0b0c0d0e0f"\n'
                         '        "00.01.02.03.04.05.06.07.08.09.0A.0b.0c.0d.0e.0f"\n'
                         '        "00 01 02 03 04 05 06 07 08 09 0A 0b 0c 0d 0e 0f"\n'
                         '        "00:01:02:03:04:05:06:07:08:09:0A:0b:0c:0d:0e:0f"\n(default: %(default)s)'
                    )

args = parser.parse_args()

if args.description:
    print(f"-> {os.path.basename(__file__):40s} {parser.description}")
    sys.exit(0)

if args.verbose:
    logging.getLogger().setLevel(logging.DEBUG)
log = logging.getLogger()

# Handle user inputs & conversions
try:
    action = actions[args.action]
    args.session = eval(args.session)
    controlees = eval(args.controlees)
    n = len(controlees) // 3
    if (n * 3) != len(controlees):
        raise SyntaxError(f'Syntax error in list of controlees. Got {args.controlees!r}')
    for i in range(n):
        k = controlees[3 * i + 2]
        if type(k) == str:
            controlees[3 * i + 2] = binascii.unhexlify(k.replace(' ', '').replace(':', '').replace('.', ''))
except Exception as e:
    print(f'Error while handling user input: {e}')
    sys.exit(uqt_errno(2))

while True:
    try:

        client = None
        client = Client(port=args.port, notif_handlers=notification_default_handlers)

        print(f'Updating the multicast list of controlee for session {args.session}...')
        rts, rtv = client.session_update_multicast_list(args.session, action, controlees)
        print(f'session_update_multicast_list: {rts.name} ({rts}).')
        if (rts != Status.Ok):
            if (rtv.count != 0): print(rtv)
            break

        if args.time == -1:
            input("Press <RETURN> to stop\n")
        else:
            time.sleep(args.time)

        break

    except UciComError as e:
        rts = e.n
        log.critical(f"{e}")
        break

if client: client.close()
if rts == Status.Ok: print('Ok')
sys.exit(uqt_errno(rts))
