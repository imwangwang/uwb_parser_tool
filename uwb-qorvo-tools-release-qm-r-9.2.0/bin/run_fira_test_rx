#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

import argparse
import logging
import time
import os

from uci import Client, App, Gid, SessionType, TestParam, Status, UciComError, TestModeSessionId
from uci import OidCore, OidTest, OidQorvo
from uci import TestDebugData, NotImplementedData, RxTestOutput

#   Below hack sometimes required when operating on windows git-bash/msys2
import sys

from utils import uqt_errno

sys.stdin.reconfigure(encoding='utf-8')
sys.stdout.reconfigure(encoding='utf-8')

parser = argparse.ArgumentParser(description="run Fira's Test RX.")
parser.add_argument("--description", action="store_true", 
                    help="show short description of the script", default=False)
parser.add_argument("-p", "--port", type=str, help='serial port used. (default: %(default)s)',
                    default=os.getenv('UQT_PORT', '/dev/ttyUSB0'))
parser.add_argument('-v', '--verbose', action='store_true',
                    help='use logging.DEBUG level. (default: %(default)s)', default=False)
parser.add_argument('-c', '--channel', type=int,
                    help='channel number. (default: %(default)s)', default=9)
parser.add_argument('--preamble-code-index', type=str, default='9',
                    help='app param. (default: %(default)s)')
parser.add_argument('--sfd-id', type=str, default='2',
                    help='app param. (default: %(default)s)')
parser.add_argument('--rframe-config', type=str, default='0',
                    help='app param. (default: %(default)s)')
parser.add_argument('--psdu-data-rate', type=str, default='0',
                    help='app param. (default: %(default)s)')
parser.add_argument('--phr-data-rate', type=str, default='0',
                    help='app param. (default: %(default)s)')
parser.add_argument('--preamble-duration', type=str, default='1',
                    help='app param. (default: %(default)s)')
parser.add_argument('--nb-sts-segments', type=str, default='0',
                    help='app param. (default: %(default)s)')
parser.add_argument('--sts-length', type=str, default='1',
                    help='Number of symbols in a STS segment. (default: %(default)s)')
parser.add_argument('--prf-mode', type=str, default='0',
                    help="Prf mode. (default: %(default)s)")
parser.add_argument('--en-diag', action='store_true', default=False,
                    help='set the Qorvo ENABLE_DIAGNOSTIC parameter to 1.')

args = parser.parse_args()

if args.description:
    print(f"-> {os.path.basename(__file__):40s} {parser.description}")
    sys.exit(0)

if args.verbose:
    logging.getLogger().setLevel(logging.DEBUG)
log = logging.getLogger()

notif_handlers = {
    (Gid.Test, OidTest.Rx): lambda x: print(RxTestOutput(x)),
    (Gid.Qorvo, OidQorvo.TestDebug): lambda x: print(TestDebugData(x)),
    ('default', 'default'): lambda gid, oid, x: print(
        f'Warning: Unexpected notification: {NotImplementedData(gid, oid, x)}')
}

while True:
    try:
        client = None
        client = Client(port=args.port, notif_handlers=notif_handlers)

        print("\n\n\n\n -----  TEST_RX -----")
        rts, session_handle = client.session_init(TestModeSessionId, SessionType.DeviceTestMode)
        print(f'{f"init TestSession {TestModeSessionId} ...":<25}{rts.name}')
        if rts != Status.Ok:
            print(f'session_init failed: {rts.name} ({rts})')
            break

        if session_handle is None:
            print(f"Using Fira 1.3 (session handle == session ID) is : {TestModeSessionId}")
            session_handle = TestModeSessionId
        else:
            print(f"Using Fira 2.0 session handle is : {session_handle}")

        time.sleep(0.5)

        rts, rtv = client.test_config_set(session_handle, [
            (TestParam.RMarkerRxStart, 0),
            (TestParam.RMarkerTxStart, 0),
            (TestParam.StsIndexAutoIncr, 0),
        ])
        print(f'{"TestConfigSet:":<25}{rts.name}')
        if rts != Status.Ok:
            print(rtv)
            client.session_deinit(session_handle)
            break
        time.sleep(0.5)

        # Session app config
        app_configs = [
            (App.DeviceMacAddress, 0x008c),
            (App.DstMacAddress, [0x008d]),
            (App.NumberOfControlees, 1),
            (App.ChannelNumber, args.channel),
            (App.MacFcsType, 0),
            (App.RframeConfig, eval(args.rframe_config)),
            (App.PreambleCodeIndex, eval(args.preamble_code_index)),
            (App.SfdId, eval(args.sfd_id)),
            (App.PsduDataRate, eval( args.psdu_data_rate)),
            (App.BprfPhrDataRate,eval( args.phr_data_rate)),
            (App.PreambleDuration, eval(args.preamble_duration)),
            (App.PrfMode, eval(args.prf_mode)),
            (App.NumberOfStsSegments, eval(args.nb_sts_segments)),
            (App.StsLength, eval(args.sts_length)),
        ]
        if args.en_diag:
            app_configs.append((App.EnableDiagnostics, 1))
        rts, rtv = client.session_set_app_config(session_handle, app_configs)
        print(f'{"TestSetAppConfig:":<25}{rts.name}')
        if rts != Status.Ok:
            print(f'session_set_app_config failed: {rts.name} ({rts}).')
            print(f'{rtv}')
            client.session_deinit(session_handle)
            break
        time.sleep(0.5)

        rts = client.test_rx()
        print(f'{"Start Rx test:":<25}{rts.name}')
        if rts != Status.Ok:
            print(f'test_rx failed: {rts.name} ({rts}).')
            client.session_deinit(session_handle)
            break

        time.sleep(2)

        rts = client.test_stop_session()
        print(f'{"Stop session:":<25}{rts.name}')
        if rts != Status.Ok:
            print(f"test_stop_session failed: {rts.name} ({rts})")
            client.session_deinit(session_handle)
            break

        time.sleep(0.5)

        rts = client.session_deinit(session_handle)
        print(f'{"Deinit TestSession::":<25}{rts.name}')
        if rts != Status.Ok:
            print(f'session_deinit failed: {rts.name} ({rts})')
            break

        time.sleep(0.5)

        break

    except UciComError as e:
        rts = e.n
        log.critical(f"{e}")
        break

if client: client.close()
if rts == Status.Ok: print('Ok')
sys.exit(uqt_errno(rts))
