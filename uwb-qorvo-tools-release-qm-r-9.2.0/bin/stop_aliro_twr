#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

import argparse
import binascii
import logging
import sys
import time
import os

from uci import *
from utils import uqt_errno

# Below hack sometimes required when operating on windows git-bash/msys2
sys.stdin.reconfigure(encoding='utf-8')
sys.stdout.reconfigure(encoding='utf-8')

parser = argparse.ArgumentParser(description="Stop a CCC Two Way Ranging session.")
parser.add_argument("--description", action="store_true", 
                    help="show short description of the script")
parser.add_argument("-p", "--port", type=str,
                    help='serial port used. (default: %(default)s)',
                    default=os.getenv('UQT_PORT', '/dev/ttyUSB0'))
parser.add_argument("--responder", action="store_true",
                    help="start in responder mode (default initiator mode)",
                    default=False)
parser.add_argument("-t", "--time", type=int,
                    help="duration of the ranging session. (default: %(default)s)", default=10)
parser.add_argument("-s", "--session_handle", dest="session_handle",
                    type=int, default=2, help="session handle")
parser.add_argument('-v', '--verbose', action='store_true',
                    help='use logging.DEBUG level. (default: %(default)s)', default=False)

args = parser.parse_args()

if args.description:
    print(f"-> {os.path.basename(__file__):40s} {parser.description}")
    sys.exit(0)

if args.verbose:
    logging.getLogger().setLevel(logging.DEBUG)
log = logging.getLogger()

while True:
    try:

        client = None
        is_alone = None
        client = Client(port=args.port)

        print(f'# Using device {args.port} as a CCC {"responser" if args.responder else "initiator"}:')

        print('# Stopping ranging:')
        rts = client.ranging_stop(args.session_handle)
        if rts != Status.Ok:
            log.critical(f'test_mode_calibrations_get() failed with status: {rts.name} ({rts})')
            break

        time.sleep(2)

        rts = client.session_deinit(args.session_handle)
        print('# Stopping session:\n    ', rts.name)
        if rts != Status.Ok:
            print(f'session_deinit failed: {rts.name} ({rts})')
            break

        break

    except UciComError as e:
        rts=e.n
        log.critical(f"{e}")
        break

if client: client.close()
sys.exit(uqt_errno(rts))