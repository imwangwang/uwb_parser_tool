#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

import argparse, logging, time, os, sys

from uci import *
from utils import uqt_errno

default_port = os.getenv('UQT_PORT', '/dev/ttyUSB0')

# Below hack sometimes required when operating on windows git-bash/msys2
sys.stdin.reconfigure(encoding="utf-8")
sys.stdout.reconfigure(encoding="utf-8")

epilog = f"""
Example of use:
    session_deinit -s 55
"""

parser = argparse.ArgumentParser(
    description="De-initialyse a FIRA session",
    formatter_class=argparse.RawTextHelpFormatter, epilog=epilog)
parser.add_argument("--description", action="store_true", 
                    help="show short description of the script")
parser.add_argument("-p", "--port", type=str, default=default_port,
                    help='communication port to use. (default: %(default)s)')
parser.add_argument('-v', '--verbose', action='store_true', default=False,
                    help='use logging.DEBUG level. (default: %(default)s)')
parser.add_argument('-s', '--session', type=str, default='42',
                    help='set a unique session handle to use or a list of sessions to allow multiple session handling in '
                         'the same uci single command script. (default: %(default)s)')
parser.add_argument('-t', '--time', type=int, default=0,
                    help='sleep time (s) before closing the UCI traffic channel\n'
                         'and exiting the script. -1: up to key pressed. (default: %(default)s)')

args = parser.parse_args()

if args.description:
    print(f"-> {os.path.basename(__file__):40s} {parser.description}")
    sys.exit(0)

if args.verbose:
    logging.getLogger().setLevel(logging.DEBUG)
log = logging.getLogger()

# Handle user inputs & conversions
try:
    sessions = eval(args.session)
    if not isinstance(sessions, (int, list)):
        raise ValueError("Invalid session format. Please provide a single session handle or a list of session handles.")
except Exception as e:
    print(f'Error while handling user input: {e}')
    sys.exit(uqt_errno(2))

if isinstance(sessions, (int, str)):
    sessions = [int(args.session)]

while sessions:
    session_handle = sessions.pop(0)
    try:

        client = None
        client = Client(port=args.port, notif_handlers=notification_default_handlers)

        print(f'Deinitializing session {session_handle}...')
        rts = client.session_deinit(session_handle)
        if rts != Status.Ok:
            print(f'session_deinit {session_handle} failed: {rts.name} ({rts})')
            break

        if args.time == -1:
            input("Press <RETURN> to stop\n")
        else:
            time.sleep(args.time)

    except UciComError as e:
        rts = e.n
        log.critical(f"{e}")
        break

if client: client.close()
if rts == Status.Ok: print('Ok')
sys.exit(uqt_errno(rts))
