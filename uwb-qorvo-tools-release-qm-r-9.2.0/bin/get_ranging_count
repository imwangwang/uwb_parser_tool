#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

import argparse, logging, os, sys

from uci import *
from utils import uqt_errno


default_port=os.getenv('UQT_PORT', '/dev/ttyUSB0')


#  Below hack sometimes required when operating on windows git-bash/msys2 
sys.stdin.reconfigure(encoding='utf-8')
sys.stdout.reconfigure(encoding='utf-8')

epilog = """
Note:

    To get the number of times ranging has been attempted during the ranging session
"""
parser = argparse.ArgumentParser(
    description="Get one or several configuration parameter value.",
    formatter_class=argparse.RawDescriptionHelpFormatter,
    epilog=epilog)
parser.add_argument("--description", action="store_true", 
                     help="show short description of the script")
parser.add_argument("-p", "--port", type=str, default=default_port,
                    help='communication port to use. (default: %(default)s)')
parser.add_argument('-v', '--verbose', action='store_true',
                    help='use logging.DEBUG level (default: %(default)s)', default=False)
parser.add_argument('-s', '--session', type=str, default='42',
        help='set a unique session id to use or a list of sessions to allow multiple session handling in the same uci '
             'single command script. (default: %(default)s)')

args = parser.parse_args()

if args.description:
    print(f"-> {os.path.basename(__file__):40s} {parser.description}")
    sys.exit(0)

if args.verbose:
    logging.getLogger().setLevel(logging.DEBUG)
log = logging.getLogger()

# Handle user inputs & conversions
try:
    sessions = eval(args.session)
    if not isinstance(sessions, (int, list)):
        raise ValueError("Invalid session format. Please provide a single session ID or a list of session IDs.")
except Exception as e:
    print(f'Error while handling user input: {e}')
    sys.exit(uqt_errno(2))

if isinstance(sessions, (int, str)):
    sessions = [int(args.session)]

notif_handlers = {
    (Gid.Ranging, OidRanging.Start): lambda x: None,
    ('default', 'default'): lambda gid, oid, x: print(NotImplementedData(gid, oid, x))
}

while sessions:
    session_id = sessions.pop(0)
    try:

        client = None
        client = Client(port=args.port, notif_handlers=notif_handlers)

        rts, rtv = client.get_ranging_count(session_id)
        print("\n# Get the number of times ranging has been attempted during the ranging session \n")

        if rts != Status.Ok:
            log.critical(f'Get the number of times ranging failed with status: {rts.name} ({rts})\n session_id: {session_id}')
            break

        print(f"Number of times ranging for session {session_id}: {rtv}")

    except UciComError as e:
        rts = e.n
        log.critical(f"{e}")
        break

if client: client.close()
sys.exit(uqt_errno(rts))
