#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""


import os
import sys
import argparse
import uci

parser = argparse.ArgumentParser(description='List available variants.')
parser.add_argument("--description", action="store_true", 
                    help="show short description of the script")
args = parser.parse_args()

if args.description:
    print(f"-> {os.path.basename(__file__):40s} {parser.description}")
    sys.exit(0)

print( f"\n# Available UCI customizations:\n    { (os.linesep+'    ').join(uci.available_customs)}")
print (f"\n# Available UCI extensions:\n    { (os.linesep+'    ').join(uci.available_addins)}")
print("\n(use 'uqt_info' to list variant in use)\n")
