#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

# Do not put in __all__ your uci Client, new Gids, or other extension objects 
# unless you want to block the addin mechanism.

__all__ = ['OidSecureElement', 'SeErrorCodes', 'SeTag',
           'BindingStatus', 'CreateRdsData', 'DoBindingData', 'GetCplcData', 'GetRdsData',
           'SeLoopbackStatus', 'get_from_ntf']

import enum, struct, time, typing
import logging

logger = logging.getLogger(__name__)

from colorama import Fore, Style, init
import uci.fira as fira
from uci.utils import *
from uci.fira import *

init()


# =============================================================================
# Additional Qorvo Enum
# =============================================================================

class Gid_extension(DynIntEnum):
    Se = 0x09


Gid.extend(Gid_extension)


class OidSecureElement(enum.IntEnum):
    SeTestLoopback = 0x16
    ClearDataPartition = 0x17
    GetBindingStatus = 0x18
    DoBinding = 0x19
    SelectSusA = 0x20
    GetRds = 0x21
    GetCPLC = 0x22
    ManageTestRds = 0x23
    GetRdsSec = 0x24
    ManageTestRdsSec = 0x25
    EraseRdsList = 0x26


class SeErrorCodes(DynIntEnum):
    SE_ERROR_OK = 0x0000
    SE_ERROR_UNKNOWN = 0x1000
    SE_ERROR_SPI_DEVICE_NOT_READY = 0x1001
    SE_ERROR_DT_SPI_NO_CS_GPIO = 0x1002
    SE_ERROR_SPI_INITIALIZATION_FAILED = 0x1003
    SE_ERROR_NO_SW = 0x1004
    SE_ERROR_SW_ERROR = 0x1005
    SE_ERROR_INVALID_PARAMETERS = 0x1006
    SE_ERROR_URSK_NOT_FOUND = 0x1007
    SE_ERROR_URSK_INVALID_SIZE = 0x1008
    SE_ERROR_NO_SUS_APPLET = 0x1009
    SE_ERROR_SE_VENDOR_CERT_FAILED = 0x100A
    SE_ERROR_SD_CERT_FAILED = 0x100B
    SE_ERROR_PSO_FAILED = 0x100C
    SE_ERROR_GET_SUS_APPLET_VERSION_FAILED = 0x100D
    SE_ERROR_SCP_MUT_AUTH_FAILED = 0x100E
    SE_ERROR_SUS_APPLET_VERSION_MISMATCH = 0x100F
    SE_ERROR_AES_SESSION_FAILED = 0x1010
    SE_ERROR_BINDING_ALREADY_DONE = 0x1011
    SE_ERROR_ISO_T1_TRANSCEIVE_FAILED = 0x1012
    SE_ERROR_SE_WRAP_FAILED = 0x1013
    SE_ERROR_SE_UNWRAP_FAILED = 0x1014
    SE_ERROR_SE_OOM = 0x1015
    # RFU 0x1011 - 0x1100
    SE_ERROR_TLV_INVALID_TAG = 0x1101
    SE_ERROR_TLV_TAG_NOT_SUPPORTED = 0x1102
    SE_ERROR_TLV_TAG_NOT_FOUND = 0x1103
    SE_ERROR_TLV_INVALID_LENGTH = 0x1104
    SE_ERROR_TLV_INVALID_DATA = 0x1105
    # RFU 0x1106 - 0xFFFF
    Unknown = 0xFFFF


class SeTag(enum.IntEnum):
    URSK = 0xA0
    CPLC = 0xA1
    SESSION_ID = 0xA2
    ERROR = 0xA3
    INST_CODE = 0xA4


class BindingStatus(enum.IntEnum):
    SeNotBound = 0
    SeBound = 1


class SeLoopbackStatus(enum.IntEnum):
    SeLoopbackFailed = 0
    SeLoopbackPass = 1


def tlv_from_bytes(enum_class, payload):
    res = []
    n = payload[0]
    p = 1

    for i in range(n):
        (t, l) = (payload[p], payload[p + 1])
        v = payload[p + 2:p + 2 + l]
        p += 2 + l
        res.append((enum_class(t), l, v))

    return res


def get_from_ntf(ntf, tag: SeTag):
    if ntf != None:
        for (t, l, v) in ntf:
            if t == tag:
                if t == SeTag.ERROR:
                    v = (int).from_bytes(v, 'little')
                    try:
                        return SeErrorCodes(v)
                    except ValueError:
                        return (SeErrorCodes.SE_ERROR_UNKNOWN)
                elif t == SeTag.CPLC:
                    return v.hex(".")
                else:
                    return v
    raise Exception(f'tag {tag.name} not found in ntf {ntf!r}')


def pop_tag(tlv_list, tag: SeTag):
    """ 
    find a tag in a list of  tlv encoded tags.
    If available, consume the tag from the list and returns its decoded value.
    return None if no tags found
    """
    idx = 0
    for (t, l, v) in tlv_list:
        if t == tag:
            tlv_list.pop(idx)
            if t == SeTag.ERROR:
                v = (int).from_bytes(v, 'little')
                try:
                    return SeErrorCodes(v)
                except ValueError:
                    return (SeErrorCodes.SE_ERROR_UNKNOWN)
            elif t == SeTag.CPLC:
                return v[::-1].hex(".")  # decoding to find
            elif t == SeTag.URSK:
                return v.hex()  # !!! to understand
            elif t == SeTag.INST_CODE:
                return v[::-1].hex(".")  # RFU(4)|Class(2)|instr(2)
            elif t == SeTag.SESSION_ID:
                return (int).from_bytes(v, 'little')
            else:  # Not possible
                logger.warning(f'"{t!r}" unknown tag.')
                return v
        idx += 1
    return None


# =============================================================================
# Additional NTF Data
# =============================================================================

class GetCplcData:
    """
    This class is encapsulating get_cplc ntf data as described somewhere.
    On R5.1-rc1-blue-build-black_acacia_b0_st54lIt, below is expected
    (depending on success or failure):
        [(<SeTag.CPLC: 161>, 42, bytearray(b'GP\x02w\x00w"\...')), (<SeTag.ERROR: 163>, 4, bytearray(b'\x00\x00\x00\x00'))]
        [(<SeTag.ERROR: 163>, 4, bytearray(b'\x12\x10\x00\x00')), (<SeTag.INST_CODE: 164>, 0, bytearray(b''))]
    """

    def __init__(self, payload: bytes):
        self.payload = payload
        tags = tlv_from_bytes(SeTag, payload) if len(payload) > 1 else None
        self.raw = tags
        self.status = pop_tag(tags, SeTag.ERROR)  # Status
        self.inst_code = 'NA'  # Instruction code in Error
        self.cplc = 'NA'  # cplc value
        if self.status == SeErrorCodes.SE_ERROR_OK:
            self.cplc = pop_tag(tags, SeTag.CPLC)
        else:  # Recover debugging information
            self.inst_code = pop_tag(tags, SeTag.INST_CODE)
        if len(tags) != 0:
            logger.warning(f'GetCplcData: unhandled remaining data: "{tags!r}"')

    def __str__(self) -> str:
        return f"""# Get CPLC:
        status:                {self.status.name} ({self.status}) 
        cplc:                  {self.cplc}
        Instr. code in error:  {self.inst_code}\n"""


class GetRdsData:
    """ 
    This class is encapsulating get rds and get rds sec ntf data as described somewhere.
    On R5.1-rc1-blue-build-black_acacia_b0_st54lIt, below is expected
    (depending on success or failure):
    [(<SeTag.URSK: 160>, 16, bytearray(b"\xed\x07\xa8\r+\xeb\x00\xf7\x85\xaf&\'\x00\x00\x00*"))]
    [(<SeTag.ERROR: 163>, 4, bytearray(b'\x05\x10\x00\x00')), (<SeTag.INST_CODE: 164>, 0, bytearray(b''))]
    """

    def __init__(self, payload: bytes):
        self.payload = payload
        tags = tlv_from_bytes(SeTag, payload) if len(payload) > 1 else None
        self.raw = tags
        self.status = SeErrorCodes.SE_ERROR_UNKNOWN  # Status
        self.inst_code = 'NA'  # Instruction code in Error
        self.ursk = 'NA'  # Session key
        status = pop_tag(tags, SeTag.ERROR)
        if status == None:  # The status is OK
            self.status = SeErrorCodes.SE_ERROR_OK
            self.ursk = pop_tag(tags, SeTag.URSK)
        else:  # Recover debugging information
            self.status = status
            self.inst_code = pop_tag(tags, SeTag.INST_CODE)
        if len(tags) != 0:
            logger.warning(f'GetRdsData: unhandled remaining data: "{tags!r}"')

    def __str__(self) -> str:
        return f"""# Get RDS Data:
        status:                {self.status.name} ({self.status}) 
        Instr. code in error:  {self.inst_code}
        ursk:                  {self.ursk}\n"""


class DoBindingData:
    """ 
    This class is encapsulating do_binding ntf data as described somewhere.
    On R5.1-rc1-blue-build-black_acacia_b0_st54lIt, below is expected
    (depending on success or failure):
    #[(<SeTag.ERROR: 163>, 4, bytearray(b'\x00\x00\x00\x00'))]
    #[(<SeTag.ERROR: 163>, 4, bytearray(b'\x05\x10\x00\x00')), (<SeTag.INST_CODE: 164>, 0, bytearray(b''))]
    """

    def __init__(self, payload: bytes):
        self.payload = payload
        tags = tlv_from_bytes(SeTag, payload) if len(payload) > 1 else None
        self.raw = tags
        self.status = pop_tag(tags, SeTag.ERROR)  # Status
        if self.status == SeErrorCodes.SE_ERROR_OK:
            self.inst_code = 'NA'  # Instruction code in Error
        else:  # Recover debugging information
            self.inst_code = pop_tag(tags, SeTag.INST_CODE)
        if len(tags) != 0:
            logger.warning(f'DoBindingData: unhandled remaining data: "{tags!r}"')

    def __str__(self) -> str:
        return f"""# Do Binding:
        status:                {self.status.name} ({self.status}) 
        Instr. code in error:  {self.inst_code}\n"""


class CreateRdsData:
    """ 
    This class is encapsulating manage_test_create_rds(_sec) ntf data as described somewhere.
    On R5.1-rc1-blue-build-black_acacia_b0_st54lIt, below is expected
    (depending on success or failure):
    #[(<SeTag.ERROR: 163>, 4, bytearray(b'\x00\x00\x00\x00'))]
    #[(<SeTag.ERROR: 163>, 4, bytearray(b'\x05\x10\x00\x00')), (<SeTag.INST_CODE: 164>, 0, bytearray(b''))]
    """

    def __init__(self, payload: bytes):
        self.payload = payload
        tags = tlv_from_bytes(SeTag, payload) if len(payload) > 1 else None
        self.raw = tags
        self.status = pop_tag(tags, SeTag.ERROR)  # Status
        if self.status == SeErrorCodes.SE_ERROR_OK:
            self.inst_code = 'NA'  # Instruction code in Error
        else:  # Recover debugging information
            self.inst_code = pop_tag(tags, SeTag.INST_CODE)
        if len(tags) != 0:
            logger.warning(f'CreateRdsData: unhandled remaining data: "{tags!r}"')

    def __str__(self) -> str:
        return f"""# Get RDS Data:
        status:                {self.status.name} ({self.status}) 
        Instr. code in error:  {self.inst_code}\n"""


def decode_se_test_ntf_loopback(payload):
    ntf_data = {}

    ntf_data['status'] = SeLoopbackStatus(
        (int).from_bytes(payload[0:1], 'little'))
    ntf_data['run_cnt'] = (int).from_bytes(payload[1:5], 'little')
    ntf_data['pass_cnt'] = (int).from_bytes(payload[5:9], 'little')
    ntf_data['pass_rate'] = (ntf_data['pass_cnt'] / ntf_data['run_cnt']) * 100
    ntf_data['tlv'] = tlv_from_bytes(SeTag, payload[9:])
    return ntf_data


def decode_get_binding_ntf(payload):
    return BindingStatus((int).from_bytes(payload, "little"))


def decode_get_cplc_ntf(payload):
    return GetCplcData(payload)


def decode_do_binding_ntf(payload):
    return DoBindingData(payload)


def decode_get_rds_ntf(payload):
    return GetRdsData(payload)


def decode_get_rds_sec_ntf(payload):
    return GetRdsData(payload)


def decode_manage_test_create_rds_ntf(payload):
    return CreateRdsData(payload)


def decode_manage_test_create_rds_sec_ntf(payload):
    return CreateRdsData(payload)


def decode_erase_rds_list_ntf(payload):
    return EraseRdsListData(payload)


class Manager:
    DEFAULT_NTFS = {
        'get_binding': {
            'decode': decode_get_binding_ntf,
            'id': (Gid.Se, OidSecureElement.GetBindingStatus)
        },
        'get_cplc': {
            'decode': decode_get_cplc_ntf,
            'id': (Gid.Se, OidSecureElement.GetCPLC)
        },
        'do_binding': {
            'decode': decode_do_binding_ntf,
            'id': (Gid.Se, OidSecureElement.DoBinding)
        },
        'se_test_loopback': {
            'decode': decode_se_test_ntf_loopback,
            'id': (Gid.Se, OidSecureElement.SeTestLoopback)
        },
        'get_rds': {
            'decode': decode_get_rds_ntf,
            'id': (Gid.Se, OidSecureElement.GetRds)
        },
        'get_rds_sec': {
            'decode': decode_get_rds_sec_ntf,
            'id': (Gid.Se, OidSecureElement.GetRdsSec)
        },
        'manage_test_create_rds': {
            'decode': decode_manage_test_create_rds_ntf,
            'id': (Gid.Se, OidSecureElement.ManageTestRds)
        },

        'manage_test_create_rds_sec': {
            'decode': decode_manage_test_create_rds_sec_ntf,
            'id': (Gid.Se, OidSecureElement.ManageTestRdsSec)
        },
        'erase_rds_list': {
            'decode': decode_erase_rds_list_ntf,
            'id': (Gid.Se, OidSecureElement.EraseRdsList)
        },
    }

    def __init__(self, ntfs=None):
        if ntfs is None:
            ntfs = Manager.DEFAULT_NTFS.keys()

        self._slots: typing.Dict[str, list] = {}
        for ntf in ntfs:
            self._slots[ntf] = []

    def _handler_factory(self, ntf: str) -> typing.Callable[[bytes], None]:
        def handle_ntf(payload: bytes) -> None:
            decoded_payload = self.DEFAULT_NTFS[ntf]['decode'](payload)

            message = (f'{Fore.MAGENTA}>>> NTF >>> {ntf}: '
                       f'{decoded_payload}{Style.RESET_ALL}')
            # logger.info(f'<<< Notification {ntf} ({self.DEFAULT_NTFS[ntf]["id"][0].name}, {self.DEFAULT_NTFS[ntf]["id"][1].name}): {decoded_payload!r}')
            logger.info(message)

            self.add_ntf(ntf, decoded_payload)

        return handle_ntf

    def generate_handlers(self) -> typing.Dict[typing.Any,
    typing.Callable[[bytes], None]]:
        handler = {}
        for ntf in self._slots:
            handler[self.DEFAULT_NTFS[ntf]['id']] = self._handler_factory(ntf)
        return handler

    def clear_ntfs(self, ntf: str):
        ntfs = self._slots[ntf]
        if len(ntfs) != 0:
            logger.warn(f'Discarding {len(ntfs)} {ntf} ntf : {ntfs!r}')
        self._slots[ntf] = []

    def get_ntfs(self, ntf: str) -> list:
        ntfs = self._slots[ntf]
        self._slots[ntf] = []
        return ntfs

    def get_ntf(self, ntf: str):
        if len(self._slots[ntf]) == 0:
            return None
        else:
            return self._slots[ntf].pop(0)

    def add_ntf(self, slot: str, decoded_data: typing.Any) -> None:
        self._slots[slot].append(decoded_data)


# =============================================================================
# Additional Client functionalities
# =============================================================================

class Client():
    def __init__(self, *args, **kwargs):
        handlers = {}
        self.ntf_manager = Manager()
        handlers.update(self.ntf_manager.generate_handlers())
        handlers.update(kwargs.get('notif_handlers', {}))
        kwargs["notif_handlers"] = handlers
        super().__init__(*args, **kwargs)

    def se_test_loopback(self, connectivity, count, interval_ms):
        payload = connectivity.to_bytes(1, 'little')
        payload += count.to_bytes(4, 'little')
        payload += interval_ms.to_bytes(4, 'little')
        payload = self.command(
            Gid.Se, OidSecureElement.SeTestLoopback, payload
        )

        return (
            Status((int).from_bytes(payload[0:1], "little"))
        )

    def clear_data_partition(self):
        payload = self.command(
            Gid.Se, OidSecureElement.ClearDataPartition, b""
        )

        return (
            Status((int).from_bytes(payload[0:1], "little"))
        )

    def get_binding_status(self):
        payload = self.command(
            Gid.Se, OidSecureElement.GetBindingStatus, b""
        )

        return (
            Status((int).from_bytes(payload[0:1], "little"))
        )

    def do_binding(self, lock):
        payload = lock.to_bytes(1, 'little')
        payload = self.command(
            Gid.Se, OidSecureElement.DoBinding, payload
        )

        return (
            Status((int).from_bytes(payload[0:1], "little"))
        )

    def get_cplc(self):
        payload = self.command(
            Gid.Se, OidSecureElement.GetCPLC, b""
        )

        return (
            Status((int).from_bytes(payload[0:1], "little"))
        )

    def select_susa(self):
        payload = self.command(
            Gid.Se, OidSecureElement.SelectSusA, b""
        )

        return (
            Status((int).from_bytes(payload[0:1], "little")),
            tlv_from_bytes(SeTag, payload[1:]) if len(payload) > 1 else None,
        )

    def get_rds(self, session_id):
        payload = (session_id).to_bytes(4, "little")

        payload = self.command(
            Gid.Se, OidSecureElement.GetRds, payload
        )

        return (
            Status((int).from_bytes(payload[0:1], "little")),
            tlv_from_bytes(SeTag, payload[1:]) if len(payload) > 1 else None,
        )

    def manage_test_create_rds(self, session_id, ursk):
        payload = struct.pack(
            "<BBBLBB",
            2,  # nb TLV
            SeTag.SESSION_ID,  # Tag
            4,  # Length
            session_id,  # Value
            SeTag.URSK,  # Tag
            len(ursk),  # Length
        )
        payload += ursk  # Value

        payload = self.command(
            Gid.Se, OidSecureElement.ManageTestRds, payload
        )

        return (
            Status((int).from_bytes(payload[0:1], "little")),
            tlv_from_bytes(SeTag, payload[1:]) if len(payload) > 1 else None,
        )

    def get_rds_sec(self, session_id):
        payload = (session_id).to_bytes(4, "little")

        payload = self.command(
            Gid.Se, OidSecureElement.GetRdsSec, payload
        )

        return (
            Status((int).from_bytes(payload[0:1], "little")),
            tlv_from_bytes(SeTag, payload[1:]) if len(payload) > 1 else None,
        )

    def manage_test_create_rds_sec(self, session_id, ursk):
        payload = struct.pack(
            "<BBBLBB",
            2,  # nb TLV
            SeTag.SESSION_ID,  # Tag
            4,  # Length
            session_id,  # Value
            SeTag.URSK,  # Tag
            len(ursk),  # Length
        )
        payload += ursk  # Value

        payload = self.command(
            Gid.Se, OidSecureElement.ManageTestRdsSec, payload
        )

        return (
            Status((int).from_bytes(payload[0:1], "little")),
            tlv_from_bytes(SeTag, payload[1:]) if len(payload) > 1 else None,
        )

    def manage_test_delete_all_rds_sec(self):
        payload = struct.pack(
            "<B",
            0,  # nb TLV
        )
        payload = self.command(
            Gid.Se, OidSecureElement.ManageTestRdsSec, payload
        )
        return (
            Status((int).from_bytes(payload[0:1], "little")),
            tlv_from_bytes(SeTag, payload[1:]) if len(payload) > 1 else None,
        )

    def erase_rds_list_sec(self, session_ids):
        payload = len(session_ids).to_bytes(1, 'little')
        for s in session_ids:
            payload += s.to_bytes(4, 'little', signed=False)
        payload = self.command(Gid.Se, OidSecureElement.EraseRdsList, payload)

        return (
            Status((int).from_bytes(payload[0:1], "little")),
            tlv_from_bytes(SeTag, payload[1:]) if len(payload) > 1 else None,
        )

    def clear_ntfs(self, ntf_name):
        self.ntf_manager.clear_ntfs(ntf_name)

    def wait_for_ntfs(self, ntf_name, timeout=2):
        POLLING_RATE = 0.2
        ntfs = self.ntf_manager.get_ntfs(ntf_name)
        recorded_time = 0
        while len(ntfs) == 0:
            logger.debug('waiting for ntf ...')
            time.sleep(POLLING_RATE)
            recorded_time += POLLING_RATE
            if recorded_time > timeout:
                raise Exception(f"Error: Timeout ({timeout}s) reached while waiting for '{ntf_name}' notification.")
            ntfs = self.ntf_manager.get_ntfs(ntf_name)
        return ntfs

    def wait_for_ntf(self, ntf_name, timeout=2):
        POLLING_RATE = 0.2
        ntf = self.ntf_manager.get_ntf(ntf_name)
        recorded_time = 0
        while ntf == None:
            logger.debug('waiting for ntf ...')
            time.sleep(POLLING_RATE)
            recorded_time += POLLING_RATE
            if recorded_time > timeout:
                raise Exception(f"Error: Timeout ({timeout}s) reached while waiting for '{ntf_name}' notification.")
            ntf = self.ntf_manager.get_ntf(ntf_name)
        return ntf


fira.Client.extend(Client)


class EraseRdsListData:
    """This class in used by the erase_rds_list_sec NTF (ERASE_RDS_LIST_NTF)"""

    class Ursk:
        def __init__(self, b: Buffer):
            tag = b.pop_uint(1)
            length = b.pop_uint(1)
            if tag == 0xA2:
                if length == 4:
                    self.session_id = b.pop_uint(length)
                else:
                    print(f"Wrong size for type {tag}, expected size 4, actual size {length} ")
            elif tag == 0xA3:
                if length == 4:
                    value = b.pop_uint(length)
                    try:
                        self.status = SeErrorCodes(value)
                    except ValueError:
                        self.status = SeErrorCodes.SE_ERROR_UNKNOWN
                else:
                    print(f"Wrong size for type {tag}, expected size 4, actual size {length} ")
            elif tag == 0xA4:
                if length == 8:
                    self.error_detail = b.pop(length)[::-1].hex(".")  # RFU(4)|Class(2)|instr(2)
                else:
                    print(f"Wrong size for type {tag}, expected size 8, actual size {length} ")
            else:
                print(f"Tag not recognized {tag}")

        def __str__(self) -> str:
            if hasattr(self, 'session_id'):
                return f"        - Session {self.session_id}\n"
            elif hasattr(self, 'status'):
                return f"        - Status {self.status}, {self.status.name}\n"
            elif hasattr(self, 'error_detail'):
                return f"        - Error Detail {self.error_detail}\n"

    def __init__(self, payload: bytes):
        self.payload = payload
        self.ursks = []
        b = Buffer(payload)
        b.pop_uint(1)
        try:
            while b.remaining_size() != 0:
                self.ursks.append(self.Ursk(b))
        except ValueError as v:
            logger.warning(f'EraseRdsListData: {b.remaining_size()} unhandled remaining bytes.\n {v}')

    def __str__(self) -> str:
        rts = f"""\n# URSK Deletion:\n"""
        for ursk in self.ursks:
            rts += str(ursk)
        return rts


# =============================================================================
# UCI Message Extension
# =============================================================================

for i in OidSecureElement:
    fira.uci_codecs[(fira.MT.Command, fira.Gid.Se, i)] = fira.default_codec(f'SE.{i.name}')
    fira.uci_codecs[(fira.MT.Response, fira.Gid.Se, i)] = fira.default_codec(f'SE.{i.name}')

fira.uci_codecs.update({
    (fira.MT.Notif, fira.Gid.Se, OidSecureElement.GetCPLC): GetCplcData,
    (fira.MT.Notif, fira.Gid.Se, OidSecureElement.GetRds): GetRdsData,
    (fira.MT.Notif, fira.Gid.Se, OidSecureElement.GetRdsSec): GetRdsData,
    (fira.MT.Notif, fira.Gid.Se, OidSecureElement.DoBinding): DoBindingData,
    (fira.MT.Notif, fira.Gid.Se, OidSecureElement.ManageTestRds): CreateRdsData,
    (fira.MT.Notif, fira.Gid.Se, OidSecureElement.ManageTestRdsSec): CreateRdsData,
    (fira.MT.Notif, fira.Gid.Se, OidSecureElement.EraseRdsList): EraseRdsListData,

})
