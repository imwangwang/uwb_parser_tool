#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

__all__= ['Config', 'config_params']

from uci.utils import DynIntEnum, Int8
from .fira_enums import DeviceState

class Config(DynIntEnum):
    State = 0x0
    LowPowerMode = 0x1

Config.defs = [
    (Config.State, 1),
    (Config.LowPowerMode, 1),
    ]

config_params = {
    # Enum Value,               parameter type, Read Only, description) 
    Config.State             : (DeviceState, 1, 'get Device State.'),
    Config.LowPowerMode      : (Int8,  0, 'get/set low power mode. 0: disable / 1: enable.'),
    }

