#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

import logging
import struct
import threading
from enum import IntEnum
from time import sleep

from uci.spi.spi_ft2232 import SpiFT2232
from uci.spi.spi_ft4222 import SpiFT4222

STC_FLAG = "<BBh"
STC_FLAG_LEN = struct.calcsize(STC_FLAG)
STC_HDR_LEN = 4

UL_FLASHING = 1
UL_UCI = 2
UL_COREDUMP = 3
UL_LOGS = 4

logger = logging.getLogger(__name__)


class Gpio_dir(IntEnum):
    IN = 0
    OUT = 1


class HostStcFlag(IntEnum):
    WR = 1 << 7
    PRD = 1 << 6
    RD = 1 << 5


class SocStcFlag(IntEnum):
    OWD = 1 << 7
    OA = 1 << 6
    RDY = 1 << 5
    ERR = 1 << 4


class HsspiTransportProtocol:
    def __init__(self, port, frequency=None):
        if 'FT4222' in port:
            self.spi = SpiFT4222(port,
                     frequency,
                     gpio_direction=(Gpio_dir.OUT, Gpio_dir.IN, Gpio_dir.IN),
                     gpio_initial=(True, False, False)
                    )
            self.ss_rdy_gpio = 2
            self.ss_irq_gpio = 1
            self.reset_gpio = 0
        else:
            self.spi = SpiFT2232(port,
                     frequency,
                     gpio_direction=(Gpio_dir.IN, Gpio_dir.IN, Gpio_dir.OUT),
                     gpio_initial=(False, False, True)
                    )

            self.ss_rdy_gpio = 0
            self.ss_irq_gpio = 1
            self.reset_gpio = 2

        self.spi.set_gpio(self.reset_gpio, True)
        self.running = False
        # Start a thread to catch messages from the slave
        self.read_thread = threading.Thread(
            target=self.wait_for_msg, daemon=True
        )

        self.lock = threading.Lock()

    def start(self):
        self.read_thread.start()

    def reset(self):
        print("Reset device")
        self.spi.set_gpio(self.reset_gpio, False)
        sleep(0.2)
        self.spi.set_gpio(self.reset_gpio, True)

    def get_ss_irq(self) -> int:
        return self.spi.get_gpio(self.ss_irq_gpio)

    def wait_ss_irq(self):
        while not((self.get_ss_irq() == 1) and (self.get_ss_rdy() == 1)):
            if not self.running: return

    def get_ss_rdy(self) -> int:
        return self.spi.get_gpio(self.ss_rdy_gpio)

    def wait_ss_rdy(self):
        while self.get_ss_rdy() != 1:
            if not self.running: return

    def construct_write(self, ul: int, payload: bytes) -> bytes:
        data = (
            struct.pack(STC_FLAG, HostStcFlag.WR, ul, len(payload)) + payload
        )
        return data

    def construct_pre_read(self, ul: int) -> bytes:
        data = struct.pack(STC_FLAG, HostStcFlag.PRD, ul, 0)
        return data

    def construct_read(self, ul: int, length: int) -> bytes:
        data = struct.pack(STC_FLAG, HostStcFlag.RD, ul, length)
        return data

    def write(self, ul: int, payload: bytes) -> bytes:
        data = self.construct_write(ul, payload)

        with self.lock:
            return self.transceive(data, STC_HDR_LEN)

    def pre_read(self) -> bytes:
        data = self.construct_pre_read(2)
        return self.transceive(data, STC_HDR_LEN)

    def read(self, ul: int, length: int) -> bytes:
        # logging.debug("read ul=%d; length=%d", ul, length)
        data = self.construct_read(ul, length)
        return self.transceive(data, length + STC_HDR_LEN)

    def transceive(self, data: bytes, length: int) -> bytes:
        self.wait_ss_rdy()
        return self.spi.transceive(data, length)

    def get_stc(self, frame) -> int:
        return frame[0]

    def get_ul(self, frame) -> int:
        return frame[1]

    def get_length(self, frame) -> int:
        return struct.unpack(STC_FLAG, frame[:STC_FLAG_LEN])[2]

    def set_msg_handler(self, handler):
        self.msg_handlers = handler

    def wait_for_msg(self):
        self.running = True
        while self.running:
            self.wait_ss_irq()
            with self.lock:
                pre_rd_msg = self.pre_read()

                resp = self.read(
                    self.get_ul(pre_rd_msg), self.get_length(pre_rd_msg)
                )

            flag = self.get_stc(resp)

            if flag & SocStcFlag.OA:
                ul = self.get_ul(resp)
                if ul == UL_UCI:
                    self.msg_handlers()(resp[STC_HDR_LEN:])
                elif ul == UL_LOGS:
                    print("QM35-LOG: " + resp[STC_HDR_LEN + 4:].decode())

    def close(self):
        self.running = False
        self.read_thread.join(timeout=1)

    def __del__(self):
        self.close()
