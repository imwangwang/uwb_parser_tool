#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

# Do not put in __all__ your uci Client, new Gids, or other extension objects 
# unless you want to block the addin mechanism.

__all__ = ['QueryGpioTimestampOutput']

from uci.utils import DynIntEnum, Buffer
from .fira_enums import Status
import uci.fira as fira
import uci.qorvo as qorvo
from dataclasses import dataclass

class Client_extension():
    def query_gpio_timestamp(self):
        payload = self.command(qorvo.Gid.Qorvo, qorvo.OidQorvo.QueryGpioTimestamp, b'')
        b=Buffer(payload)
        rts=Status(b.pop_uint(1))
        seq_number=b.pop_uint(1)
        timestamp_us=b.pop_int(8)
        return (rts, seq_number, timestamp_us)
fira.Client.extend(Client_extension)


class OidQorvo_extension(DynIntEnum):
    QueryGpioTimestamp       = 0x36  # QUERY_GPIO_TIMESTAMP_CMD/RSP
qorvo.OidQorvo.extend(OidQorvo_extension)


@dataclass
class QueryGpioTimestampOutput:
    """
    QUERY_GPIO_TIMESTAMP_CMD data.
    """
    def __init__(self, payload: bytes):
        self.gid=qorvo.Gid.Qorvo
        self.oid=qorvo.OidQorvo.QueryGpioTimestamp
        self.payload=payload
        b=Buffer(payload)
        self.status=Status(b.pop_uint(1))
        self.seq_number=b.pop_uint(1)
        self.timestamp_us=b.pop_int(8)
    def __str__(self) -> str:
        if self.status == Status.Success:
            rts= f"""# QUERY_GPIO_TIMESTAMP_CMD:
            status: {self.status.name} ({self.status.value})
            seq_number: {self.seq_number}
            timestamp_us: {self.timestamp_us}\n"""
        else:
            rts= f"""# QUERY_GPIO_TIMESTAMP_CMD:
            status: {self.status.name})\n"""
        return rts

# =============================================================================
# UCI Message Extension
# =============================================================================

fira.uci_codecs.update({
    (fira.MT.Command, qorvo.Gid.Qorvo, qorvo.OidQorvo.QueryGpioTimestamp):  fira.default_codec("Query GPIO_Timestamp", no_data = True ),
    (fira.MT.Response, qorvo.Gid.Qorvo, qorvo.OidQorvo.QueryGpioTimestamp): QueryGpioTimestampOutput
})
