#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

"""
    This library is implementing uci cmd & ntf related to the Android adaptation layer
    as defined at:
     - https://source.android.com/docs/core/connect/uwb-hal-interface
     - https://cs.android.com/android/platform/superproject/+/master:hardware/interfaces/uwb/aidl/android/hardware/uwb/fira_android/
    ... but in sync with current Qorvo implementation.
"""

__all__ = ['OidAndroid']

from dataclasses import dataclass

from uci.utils import *
import uci.fira as fira
import uci.qorvo as qorvo


# =============================================================================
# Additional Enums
# =============================================================================

class Gid_extension(DynIntEnum):
    Android = 0x0C


fira.Gid.extend(Gid_extension)


class OidAndroid(DynIntEnum):
    PowerStats = 0x0  # ANDROID_GET_POWER_STATS
    CountryCode = 0x01  # ANDROID_SET_COUNTRY_CODE
    Diagnostics = 0x02  # ANDROID_RANGE_DIAGNOSTICS
    # Range 0x10 - 0x1F reserved for RADAR specific
    RadarSetAppConfig = 0x11  # RADAR_SET_APP_CONFIG
    RadarGetAppConfig = 0x12,  # RADAR_GET_APP_CONFIG
    RadarDataNtf = 0x13,  # RADAR_DATA_NTF


# =============================================================================
# Notifications
# =============================================================================

@dataclass
class AndroidRangingDiagData(qorvo.RangingDiagData):
    """ 
    This class is encapsulating AndroidRangeDiagnosticsNtf data
    as described in https://cs.android.com/android/platform/superproject/+/master:external/uwb/src/rust/uwb_uci_packets/uci_packets.pdl;l=1301?q=rangediagnostics
    """
    gid = fira.Gid.Android
    oid = OidAndroid.Diagnostics


fira.notification_default_handlers.update({
    (fira.Gid.Android, OidAndroid.Diagnostics): lambda x: print(AndroidRangingDiagData(x))
})

# =============================================================================
# UCI Message Extension
# =============================================================================


fira.uci_codecs.update({
    (fira.MT.Notif, fira.Gid.Android, OidAndroid.Diagnostics): AndroidRangingDiagData
})


# =============================================================================
# UCI Object Capability Extension
# =============================================================================

class CapsParameters_extension(DynIntEnum):
    AOSP_CCC_SUPPORTED_CHAPS_PER_SLOT = 0xA0
    AOSP_CCC_SUPPORTED_SYNC_CODES = 0xA1
    AOSP_CCC_SUPPORTED_HOPPING_CONFIG_MODES_AND_SEQUENCES = 0xA2
    AOSP_CCC_SUPPORTED_CHANNELS = 0xA3
    AOSP_CCC_SUPPORTED_VERSIONS = 0xA4
    AOSP_CCC_SUPPORTED_UWB_CONFIGS = 0xA5
    AOSP_CCC_SUPPORTED_PULSE_SHAPE_COMBOS = 0xA6
    AOSP_CCC_SUPPORTED_RAN_MULTIPLIER = 0xA7
    AOSP_CCC_SUPPORTED_MAX_RANGING_SESSION_NUMBER = 0xA8
    AOSP_CCC_SUPPORTED_MIN_UWB_INITIATION_TIME_MS = 0xA9
    AOSP_CCC_PRIORITIZED_CHANNEL_LIST = 0xAA

    AOSP_RADAR_SUPPORT = 0xB0

    AOSP_SUPPORTED_POWER_STATS_QUERY = 0xC0
    AOSP_SUPPORTED_AOA_RESULT_REQ_ANTENNA_INTERLEAVING = 0xE3
    AOSP_SUPPORTED_MIN_RANGING_INTERVAL_MS = 0xE4
    AOSP_SUPPORTED_RANGE_DATA_NTF_CONFIG = 0xE5
    AOSP_SUPPORTED_RSSI_REPORTING = 0xE6
    AOSP_SUPPORTED_DIAGNOSTICS = 0xE7
    AOSP_SUPPORTED_MIN_SLOT_DURATION = 0xE8
    AOSP_SUPPORTED_MAX_RANGING_SESSION_NUMBER = 0xE9
    AOSP_SUPPORTED_CHANNELS_AOA = 0xEA


fira.CapsParameters.extend(CapsParameters_extension)


class SupportedAoaResultReqAntennaInterleaving:
    def __init__(self, value):
        self.name = CapsParameters_extension.AOSP_SUPPORTED_AOA_RESULT_REQ_ANTENNA_INTERLEAVING.name
        self.supported = bool(value == b'\x01')

    def __str__(self):
        output = f"{self.name}\n"
        output += f"    Antenna Interleaving Supported: {'Yes' if self.supported else 'No'}\n"
        return output


class SupportedPowerStatsQuery:
    def __init__(self, value):
        self.name = CapsParameters_extension.AOSP_SUPPORTED_POWER_STATS_QUERY.name
        self.supported = bool(value == b'\x01')

    def __str__(self):
        output = f"{self.name}\n"
        output += f"    Power Stats Query Supported: {'Yes' if self.supported else 'No'}\n"
        return output


class SupportedChapsPerSlot:
    def __init__(self, value):
        value = int.from_bytes(value, byteorder='little', signed=False)
        self.name = "CCC_SUPPORTED_CHAPS_PER_SLOT"
        self.supported_values = {
            0x01: "3",
            0x02: "4",
            0x04: "6",
            0x08: "8",
            0x10: "9",
            0x20: "12",
            0x40: "24",
            0x80: "Reserved"
        }
        self.supported_slots = []

        for bit_value, slot_duration in self.supported_values.items():
            if value & bit_value:
                self.supported_slots.append(slot_duration)

    def __str__(self):
        output = f"{self.name}\n"
        output += "    Supported Slot Durations: "
        if self.supported_slots:
            output += ", ".join(self.supported_slots)
        else:
            output += "None"
        output += "\n"
        return output


class SupportedSyncCodes:
    def __init__(self, value):
        value = int.from_bytes(value, byteorder='little', signed=False)
        self.name = "CCC_SUPPORTED_SYNC_CODES"
        self.supported_indices = []

        for i in range(1, 33):
            bit_value = 1 << (i - 1)
            if value & bit_value:
                self.supported_indices.append(str(i))

    def __str__(self):
        output = f"{self.name}\n"
        output += "    Supported SYNC Code Indices: "
        if self.supported_indices:
            output += ", ".join(self.supported_indices)
        else:
            output += "None"
        output += "\n"
        return output


class SupportedHoppingConfigModesSequences:
    def __init__(self, value):
        value = int.from_bytes(value, byteorder='little', signed=False)
        self.name = "CCC_SUPPORTED_HOPPING_CONFIG_MODES_AND_SEQUENCES"
        self.supported_modes = []
        self.supported_sequences = []

        hopping_modes_mask = (value >> 5) & 0b111
        if hopping_modes_mask & 0b100:
            self.supported_modes.append("No Hopping")
        if hopping_modes_mask & 0b010:
            self.supported_modes.append("Continuous Hopping")
        if hopping_modes_mask & 0b001:
            self.supported_modes.append("Adaptive Hopping")

        hopping_sequences_mask = value & 0b11111
        if hopping_sequences_mask & 0b10000:
            self.supported_sequences.append("Default Hopping Sequence")
        if hopping_sequences_mask & 0b01000:
            self.supported_sequences.append("AES-Based Hopping Sequence")

    def __str__(self):
        output = f"{self.name}\n"
        output += "    Supported Hopping Modes: "
        if self.supported_modes:
            output += ", ".join(self.supported_modes)
        else:
            output += "None"
        output += "\n    Supported Hopping Sequences: "
        if self.supported_sequences:
            output += ", ".join(self.supported_sequences)
        else:
            output += "None"
        output += "\n"
        return output


class SupportedChannels:
    def __init__(self, value):
        value = int.from_bytes(value, byteorder='little', signed=False)
        self.name = "CCC_SUPPORTED_CHANNELS"
        self.supported_channels = []

        if value & 0x01:
            self.supported_channels.append("Channel 5")
        if value & 0x02:
            self.supported_channels.append("Channel 9")

    def __str__(self):
        output = f"{self.name}\n"
        output += "    Supported UWB Channels: "
        if self.supported_channels:
            output += ", ".join(self.supported_channels)
        else:
            output += "None"
        output += "\n"
        return output


class SupportedVersions:
    def __init__(self, value):
        self.supported_versions = []
        for i in range(0, len(value), 2):
            major_version = value[i]
            minor_version = value[i + 1]
            self.supported_versions.append((major_version, minor_version))

    def __str__(self):
        output = "Supported CCC Versions:\n"
        for major, minor in self.supported_versions:
            output += f"    Version {major}.{minor}\n"
        return output


class SupportedUwbConfigs:
    def __init__(self, value):
        self.supported_configs = []

        for byte in value:
            self.supported_configs.append(byte)

    def get_config_description(self, config):
        config_descriptions = {
            0x00: "Mandatory for Device and Vehicle",
            0x01: "Mandatory for Device, Optional for Vehicle",
        }
        return config_descriptions.get(config, "Unknown Configuration")

    def __str__(self):
        output = "Supported UWB Configurations:\n"
        for config in self.supported_configs:
            description = self.get_config_description(config)
            output += f"    Configuration 0x{config:02X}: {description}\n"
        return output


class SupportedPulseShapeCombos:
    def __init__(self, value):
        self.supported_combos = []

        for byte in value:
            initiator_tx = (byte >> 4) & 0x0F
            responder_tx = byte & 0x0F
            self.supported_combos.append((initiator_tx, responder_tx))

    def get_pulse_shape_description(self, pulse_shape):
        pulse_shape_descriptions = {
            0: "Symmetrical Root Raised Cosine",
            1: "Precursor Free",
            2: "Precursor Free Special",
        }
        return pulse_shape_descriptions.get(pulse_shape, "Unknown Pulse Shape")

    def __str__(self):
        output = "Supported Pulse Shape Combos:\n"
        for i, (initiator_tx, responder_tx) in enumerate(self.supported_combos):
            initiator_description = self.get_pulse_shape_description(initiator_tx)
            responder_description = self.get_pulse_shape_description(responder_tx)
            hex_value = f"0x{(initiator_tx << 4 | responder_tx):02X}"
            output += f"    #{i + 1} - Value {hex_value}: Initiator TX: {initiator_description}, Responder TX: {responder_description}\n"
        return output


class SupportedRanMultiplier:
    def __init__(self, value):

        self.supported = int.from_bytes(value, byteorder='little', signed=False)

    def __str__(self):
        output = f"Supported RAN Multiplier: {self.supported} \n"
        return output


class SupportedMaxRangingSessionNumberCcc:
    def __init__(self, value):
        self.supported = int.from_bytes(value, byteorder='little', signed=False)

    def __str__(self):
        output = f"Supported Max Ranging Session Number CCC: {self.supported}"
        return output


class SupportedMinUwbInitTime:
    def __init__(self, value):
        self.supported_time_ms = int.from_bytes(value, byteorder='little', signed=False)

    def __str__(self):
        output = f"Supported Min UWB Initiation Time (ms): {self.supported_time_ms}"
        return output


class PrioritizedChannelList:
    def __init__(self, value):
        self.bytes = value

    def __str__(self):
        output = "Prioritized Channel List (Highest to Lowest Priority):\n"
        for i, byte_value in enumerate(self.bytes):
            channel_number = i + 1
            output += f"    Priority {channel_number}: Channel {int.from_bytes(byte_value, byteorder='little', signed=False)}\n"
        return output


class RadarSupport:
    def __init__(self, value):
        self.name = "RADAR_SUPPORT"
        self.supported_types = []

        radar_types = {
            0x01: "Radar Sweep Samples"
            # Add other radar data types here if necessary
        }
        for byte_value in value:
            for bit_position in range(8):
                bit_value = 1 << bit_position
                if byte_value & bit_value:
                    radar_type = radar_types.get(bit_value, f"Unknown Type {bit_position + 1}")
                    self.supported_types.append(radar_type)

    def __str__(self):
        output = f"{self.name}\n"
        output += "    Supported Radar Data Types: "
        if self.supported_types:
            output += ", ".join(self.supported_types)
        else:
            output += "None"
        return output


class SupportedMinRangingInterval:
    def __init__(self, value_bytes):
        self.name = "SUPPORTED_MIN_RANGING_INTERVAL_MS"
        self.value = int.from_bytes(value_bytes, byteorder='little', signed=False)

    def __str__(self):
        output = f"{self.name}\n"
        output += f"    Supported Minimum Ranging Interval: {self.value} ms\n"
        return output


class SupportedRangeDataNtfConfig:
    def __init__(self, value_bytes):
        self.name = "SUPPORTED_RANGE_DATA_NTF_CONFIG"
        self.bitmask = int.from_bytes(value_bytes, byteorder='little', signed=False)

    def is_bit_set(self, bit_position):
        return (self.bitmask & (1 << bit_position)) != 0

    def __str__(self):
        output = f"{self.name}\n"
        output += "    Supported RANGE_DATA_NTF_CONFIG values:\n"
        for bit_position in range(32):
            if self.is_bit_set(bit_position):
                output += f"        Bit {bit_position}: Supported\n"
            else:
                output += f"        Bit {bit_position}: Not Supported\n"
        return output


class SupportedRssiReporting:
    def __init__(self, value):
        self.name = "SUPPORTED_RSSI_REPORTING"
        self.supported = bool(value == b'\x01')

    def __str__(self):
        output = f"{self.name}\n"
        output += f"    RSSI Reporting Supported: {'Yes' if self.supported else 'No'}\n"
        return output


class SupportedDiagnostics:
    def __init__(self, value):
        self.name = "SUPPORTED_DIAGNOSTICS"
        self.supported = bool(value == b'\x01')

    def __str__(self):
        output = f"{self.name}\n"
        output += f"    Diagnostics Feature Supported: {'Yes' if self.supported else 'No'}\n"
        return output


class SupportedMinSlotDuration:
    def __init__(self, value):
        self.name = "SUPPORTED_MIN_SLOT_DURATION_RSTU"
        self.value = int.from_bytes(value, byteorder='little', signed=False)

    def __str__(self):
        output = f"{self.name}\n"
        output += f"    Supported Minimum Slot Duration: {self.value} rstu\n"
        return output


class SupportedMaxRangingSessionNumber:
    def __init__(self, value):
        self.value = int.from_bytes(value, byteorder='little', signed=False)

    def __str__(self):
        output = f"Supported Max Ranging Session Number: {self.value}"
        return output


class SupportedChannelsAoa:
    def __init__(self, value_bytes):
        self.name = "SUPPORTED_CHANNELS_AOA"
        self.value = int.from_bytes(value_bytes, byteorder='little', signed=False)
        self.supported_channels = []

        channel_mapping = {
            0x01: "Channel 5",
            0x02: "Channel 6",
            0x04: "Channel 8",
            0x08: "Channel 9",
            0x10: "Channel 10",
            0x20: "Channel 12",
            0x40: "Channel 13",
            0x80: "Channel 14"
        }

        for bit_value, channel_name in channel_mapping.items():
            if self.value & bit_value:
                self.supported_channels.append(channel_name)

    def __str__(self):
        output = f"{self.name}\n"
        output += "    Supported AoA Channels: "
        if self.supported_channels:
            output += ", ".join(self.supported_channels)
        else:
            output += "None"
        return output


fira.caps.update({
    CapsParameters_extension.AOSP_SUPPORTED_AOA_RESULT_REQ_ANTENNA_INTERLEAVING: SupportedAoaResultReqAntennaInterleaving,
    CapsParameters_extension.AOSP_SUPPORTED_POWER_STATS_QUERY: SupportedPowerStatsQuery,

    CapsParameters_extension.AOSP_CCC_SUPPORTED_CHAPS_PER_SLOT: SupportedChapsPerSlot,
    CapsParameters_extension.AOSP_CCC_SUPPORTED_SYNC_CODES: SupportedSyncCodes,
    CapsParameters_extension.AOSP_CCC_SUPPORTED_HOPPING_CONFIG_MODES_AND_SEQUENCES: SupportedHoppingConfigModesSequences,
    CapsParameters_extension.AOSP_CCC_SUPPORTED_CHANNELS: SupportedChannels,
    CapsParameters_extension.AOSP_CCC_SUPPORTED_VERSIONS: SupportedVersions,
    CapsParameters_extension.AOSP_CCC_SUPPORTED_UWB_CONFIGS: SupportedUwbConfigs,
    CapsParameters_extension.AOSP_CCC_SUPPORTED_PULSE_SHAPE_COMBOS: SupportedPulseShapeCombos,
    CapsParameters_extension.AOSP_CCC_SUPPORTED_RAN_MULTIPLIER: SupportedRanMultiplier,
    CapsParameters_extension.AOSP_CCC_SUPPORTED_MAX_RANGING_SESSION_NUMBER: SupportedMaxRangingSessionNumberCcc,
    CapsParameters_extension.AOSP_CCC_SUPPORTED_MIN_UWB_INITIATION_TIME_MS: SupportedMinUwbInitTime,
    CapsParameters_extension.AOSP_CCC_PRIORITIZED_CHANNEL_LIST: PrioritizedChannelList,
    CapsParameters_extension.AOSP_RADAR_SUPPORT: RadarSupport,
    CapsParameters_extension.AOSP_SUPPORTED_MIN_RANGING_INTERVAL_MS: SupportedMinRangingInterval,
    CapsParameters_extension.AOSP_SUPPORTED_RANGE_DATA_NTF_CONFIG: SupportedRangeDataNtfConfig,
    CapsParameters_extension.AOSP_SUPPORTED_RSSI_REPORTING: SupportedRssiReporting,
    CapsParameters_extension.AOSP_SUPPORTED_DIAGNOSTICS: SupportedDiagnostics,
    CapsParameters_extension.AOSP_SUPPORTED_MIN_SLOT_DURATION: SupportedMinSlotDuration,
    CapsParameters_extension.AOSP_SUPPORTED_MAX_RANGING_SESSION_NUMBER: SupportedMaxRangingSessionNumber,

    CapsParameters_extension.AOSP_SUPPORTED_CHANNELS_AOA: SupportedChannelsAoa

})
