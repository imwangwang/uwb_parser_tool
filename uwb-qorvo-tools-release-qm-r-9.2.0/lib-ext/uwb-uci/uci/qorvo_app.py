#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

"""
    This library is handling Qorvo Session parameter definition
"""

from uci.utils import DynIntEnum
from uci.fira import App, TestParam

class App_extension(DynIntEnum): 
    NbOfRangeMeasurements = 0xe3,
    NbOfAzimuthMeasurements = 0xe4,
    NbOfElevationMeasurements = 0xe5,
    RxAntennaSelection = 0xe6,
    TxAntennaSelection = 0xe7,
    EnableDiagnostics = 0xe8,
    DiagsFrameReportsFields = 0xe9,
    MacPayloadEncryption = 0xea,
    EnablePSDUDump = 0xeb,
App.extend(App_extension)


App.defs.extend([
    (App.NbOfRangeMeasurements, 1),
    (App.NbOfAzimuthMeasurements, 1),
    (App.RxAntennaSelection, 1),
    (App.TxAntennaSelection, 1),
    (App.EnableDiagnostics, 1),
    (App.DiagsFrameReportsFields, 1),
    (App.NbOfElevationMeasurements, 1),
    (App.MacPayloadEncryption, 1),
    (App.EnablePSDUDump, 1),
    ])

class TestParam_extension(DynIntEnum):
    RssiOutliers = 0xeb
TestParam.extend(TestParam_extension)

TestParam.defs[TestParam.RssiOutliers]=2
