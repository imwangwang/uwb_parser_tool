# Copyright (c) 2023 Qorvo US, Inc.

import logging
import struct
import threading
from enum import IntEnum
from time import sleep

from uci.spi.spi_ft2232 import SpiFT2232
from uci.spi.spi_ft4222 import SpiFT4222

from pyft4222.wrapper.gpio import GpioTrigger

UL_FLASHING = 1
UL_UCI = 2
UL_COREDUMP = 3
UL_LOGS = 4

logger = logging.getLogger(__name__)

# struct hsspi_transport_buffer_header {
#     uint32_t magic : 24;
#     uint8_t ul;
#     uint16_t len;
#     uint16_t next_len_hint;
# } __attribute__((packed));
# payload
# [footer]
# #define HSSPI_TRANSPORT_HEADER_MAGIC 0xABCDEF
# #define HSSPI_TRANSPORT_FOOTER_MAGIC 0x89ABCDEF
HSSPI_TRANSPORT_HEADER_MAGIC_SIZE = 3
HSSPI_TRANSPORT_HEADER_SIZE = 8
HSSPI_TRANSPORT_FOOTER_SIZE = 4
HSSPI_TRANSPORT_HEADER_MAGIC_BYTES = int(0xABCDEF).to_bytes(HSSPI_TRANSPORT_HEADER_MAGIC_SIZE, 'little')
HSSPI_TRANSPORT_FOOTER_MAGIC_BYTES = int(0x89ABCDEF).to_bytes(HSSPI_TRANSPORT_FOOTER_SIZE, 'little')
HSSPI_TRANSPORT_OVERHEAD_SIZE = HSSPI_TRANSPORT_HEADER_SIZE + HSSPI_TRANSPORT_FOOTER_SIZE
TRANSPORT_HEADER_PACK_FMT = "<BHH"
DEFAULT_NEXT_LEN_HINT = 200
DEFAULT_MAX_HSSPI_LEN = 2048
MAX_TRANSPORT_PAYLOAD_LEN = DEFAULT_MAX_HSSPI_LEN - HSSPI_TRANSPORT_OVERHEAD_SIZE

def debug_log(s):
    # logger.debug(s)
    pass

class Gpio_dir(IntEnum):
    IN = 0
    OUT = 1

class HsspiTransportProtocolV2:
    def __init__(self, port):
        self.read_thread = None
        self.current_cond = None
        self.lock = None
        if 'FT4222' not in port:
            raise ValueError('Only FT4222 supported')
        needle_idx = port.find('@')
        # default to 1Mbps
        frequency = 1000000
        if needle_idx != -1:
            frequency = int(port[needle_idx+1:])
            port = port[:needle_idx]
        self.spi = SpiFT4222(port,
                    frequency,
                    gpio_direction=(Gpio_dir.OUT, Gpio_dir.IN, Gpio_dir.IN),
                    gpio_initial=(True, False, False)
                )
        self.ss_rdy_gpio = 2
        self.ss_irq_gpio = 1
        self.exton_gpio = 3
        self.reset_gpio = 0

        self.spi.set_gpio(self.reset_gpio, True)
        self.running = False
        self.next_len_hint = DEFAULT_NEXT_LEN_HINT
        # Start a thread to catch messages from the slave
        self.read_thread = threading.Thread(
            target=self.wait_for_msg, daemon=True
        )

        self.spi._gpio.set_input_trigger(self.ss_rdy_gpio, GpioTrigger.RISING)

        self.tx_queue = list()
        self.lock = threading.Lock()
        self.ss_rdy_initial = self.get_ss_rdy()
        self.exton_initial = self.get_exton()

    def start(self):
        self.read_thread.start()

    def reset(self):
        print("Reset device")
        self.spi.set_gpio(self.reset_gpio, False)
        sleep(0.2)
        self.spi.set_gpio(self.reset_gpio, True)

    def get_ss_irq(self) -> int:
        return self.spi.get_gpio(self.ss_irq_gpio)

    def get_exton(self) -> int:
        return self.spi.get_gpio(self.exton_gpio)

    def wait_next_xfer(self) -> tuple[bytes, threading.Condition]:
        with self.lock:
            if len(self.tx_queue) > 0:
                debug_log(f"Returning some data to send")
                return self.tx_queue.pop()
        # no data to send yet, wait for the soc to have something
        # to send or ourselves to have something to send in our
        # tx_queue
        while self.get_ss_irq() == 0:
            if not self.running:
                break
            with self.lock:
                if len(self.tx_queue) > 0:
                    debug_log(f"Returning some data to send")
                    return self.tx_queue.pop()
        debug_log(f"Returning some data to fetch")
        return [None, None]

    def get_ss_rdy(self) -> int:
        return self.spi.get_gpio(self.ss_rdy_gpio)

    def construct_transport_msg(self, ul: int, payload: bytes) -> bytes:
        if ul > 0:
            data = (
                HSSPI_TRANSPORT_HEADER_MAGIC_BYTES + struct.pack(TRANSPORT_HEADER_PACK_FMT,
                            ul, len(payload), 0) + payload + HSSPI_TRANSPORT_FOOTER_MAGIC_BYTES
            )
        else:
            # Build an empty msg long enough to fit the data the QM is sending
            data = (
                HSSPI_TRANSPORT_HEADER_MAGIC_BYTES + struct.pack(TRANSPORT_HEADER_PACK_FMT,
                            0, 0, 0) + HSSPI_TRANSPORT_FOOTER_MAGIC_BYTES + payload
            )
        return data
    
    def wake_qm(self, nb_events = None):
        # Flush the events queue first
        # if the QM wakes before we wake it up,
        # we should have another ss-rdy event anyway
        nb_events = self.spi._gpio.get_queued_trigger_event_count(self.ss_rdy_gpio)
        if nb_events > 0:
            self.spi._gpio.read_trigger_queue(self.ss_rdy_gpio, nb_events)
        # Wake the QM
        self.spi.transceive(bytes([0]), 1)
    
    def wait_ss_rdy(self):
        if self.ss_rdy_initial == 1:
            self.ss_rdy_initial = 0
            return
        debug_log(f"Waiting on ss_rdy")
        if self.get_exton() == 0:
            self.wake_qm(None)
        while True:
            nb_events = self.spi._gpio.get_queued_trigger_event_count(self.ss_rdy_gpio)
            if nb_events > 0:
                self.spi._gpio.read_trigger_queue(self.ss_rdy_gpio, nb_events)
                break
        debug_log(f"ss_rdy received")

    def is_msg_valid(self, data) -> list:
        if not data or len(data) < HSSPI_TRANSPORT_OVERHEAD_SIZE:
            if data:
                logger.error(f'Not enough bytes received {len(data)}')
            return None
        header_magic = data[:HSSPI_TRANSPORT_HEADER_MAGIC_SIZE]
        ul, header_len, header_next_len = struct.unpack(TRANSPORT_HEADER_PACK_FMT, data[HSSPI_TRANSPORT_HEADER_MAGIC_SIZE:HSSPI_TRANSPORT_HEADER_SIZE])
        if header_magic != HSSPI_TRANSPORT_HEADER_MAGIC_BYTES:
            if sum(header_magic) != 0:
                logger.error(f'Wrong header magic {header_magic}')
            else:
                logger.error(f'Wrong header 0 magic {header_magic}')
            return None
        if header_len > len(data) - HSSPI_TRANSPORT_OVERHEAD_SIZE:
            logger.error(f'Wrong message len {len(data)}, should have been {header_len + HSSPI_TRANSPORT_OVERHEAD_SIZE}')
            # update the next len hint in case we didn't transfer enough bytes
            self.next_len_hint = max(header_len, DEFAULT_NEXT_LEN_HINT)
            return None
        if HSSPI_TRANSPORT_FOOTER_MAGIC_BYTES != data[HSSPI_TRANSPORT_HEADER_SIZE + header_len:HSSPI_TRANSPORT_HEADER_SIZE + header_len + HSSPI_TRANSPORT_FOOTER_SIZE]:
            logger.error(f'Wrong footer {data[HSSPI_TRANSPORT_HEADER_SIZE + header_len:]}')
            return None
        if header_next_len != 0:
            self.next_len_hint = max(header_next_len, DEFAULT_NEXT_LEN_HINT)
        payload = data[ HSSPI_TRANSPORT_HEADER_SIZE:HSSPI_TRANSPORT_HEADER_SIZE+header_len ]
        return ( ul, payload )

    def write(self, ul: int, payload: bytes) -> bytes:
        data = self.construct_transport_msg(ul, payload)
        cond = threading.Condition(threading.Lock())
        debug_log(f"Posting some data to xfer")
        with self.lock:
            self.tx_queue.append([data, cond])
        debug_log(f"Data posted")
        with cond:
            cond.wait()

        return None

    def set_msg_handler(self, handler):
        self.msg_handlers = handler

    def wait_for_msg(self):
        self.running = True
        empty_payload = bytearray(DEFAULT_MAX_HSSPI_LEN)
        debug_log(f"thread id: {threading.get_native_id()}")
        while self.running:
            debug_log(f"Waiting for the next xfer")
            tx_data, cond = self.wait_next_xfer()
            debug_log(f"Process next xfer")
            self.current_cond = cond
            nlh = self.next_len_hint
            if tx_data == None:
                tx_data = self.construct_transport_msg(0, empty_payload[:self.next_len_hint])
            l = len(tx_data)
            if l < self.next_len_hint:
                l = self.next_len_hint
            debug_log(f"Waiting ss ready")
            rx_data = None
            while not rx_data or sum(rx_data[0:4]) == 0:
                self.wait_ss_rdy()
                debug_log(f"Sending message")
                rx_data = self.spi.transceive(tx_data, l)

            res = self.is_msg_valid(rx_data)
            if res:
                debug_log(f"Message sent")
                if cond:
                    debug_log(f"Notifying cond {cond}")
                    with cond:
                        cond.notify_all()
                ( ul, payload ) = res
                if ul == UL_UCI:
                    self.msg_handlers()(payload)
                elif ul == UL_LOGS:
                    logger.info("QM35-LOG: " + payload.decode(), end='')
                else:
                    logger.debug(f"Unknown UL {ul}")
            else:
                debug_log(f"Message invalid rx_data {len(rx_data)} bytes, tx_data {len(tx_data)}, next_len_hint {nlh}")



    def close(self):
        self.running = False
        # Release the writers as well
        if self.lock:
            with self.lock:
                while len(self.tx_queue) > 0:
                    _, cond = self.tx_queue.pop()
                    logger.error(f"CLOSE: Notifying cond {cond}")
                    with cond:
                        cond.notify_all()
        if self.current_cond:
            logger.info(f"CLOSE: Notifying current cond {self.current_cond}")
            with self.current_cond:
                self.current_cond.notify_all()

        if self.read_thread:
            self.read_thread.join(timeout=1)

    def __del__(self):
        self.close()
