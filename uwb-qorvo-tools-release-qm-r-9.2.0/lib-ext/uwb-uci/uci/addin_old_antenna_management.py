#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

# Do not put in __all__ your uci Client, new Gids, or other extension objects 
# unless you want to block the addin mechanism.

__all__ = []

from uci.utils import *
from uci.qorvo import *
from uci.qorvo import build_cal_params, dot, CalibrationParams, cal_params


class CalibrationParams_extension(DynIntEnum):
    TxData = 0x37,
    TxSts = 0x38,
    Caps = 0x3A,
    TxPower = 0x1,
    LutId = 0x25,
    AntGrpRfCfgRfOff = 0x0B,
    AntGrpRfCfgTx = 0x0C,
    AntGrpRfCfgTxAoa = 0x0D,
    AntGrpRfCfgRxIp = 0x0E,
    AntGrpRfCfgRxSts0 = 0xF,
    AntGrpRfCfgRxSts1 = 0x10,
    AntGrpRfCfgRxSts2 = 0x11,
    AntGrpRfCfgRxSts3 = 0x12,
    AntGrpLnaRxA = 0x13,
    AntGrpLnaRxB = 0x14,
    AntGrpPa = 0x15,
    AntGrpStsMode = 0x16,
    AntGrpRxCfg = 0x17,
    AntGrpPDoASegments = 0x19
    AntGrpCfgMode = 0x1B,
    AntGrpFixedRfCfgTx = 0x1C,
    AntGrpFixedRfCfgRx = 0x1D,
    ExtSwConfig = 0x1E,
    AntGrpPDoAType = 0x1F,
    AntConfig = 0x3D,
    RssiOffSetQ3 = 0x4F
    ExperimentalMacSession_SchedulerId = 0x4E

CalibrationParams.extend(CalibrationParams_extension)


new_antenna_to_remove = [
    (CalibrationParams.Transceiver, 1, r"ant\d+\.transceiver$"),
    (CalibrationParams.Port, 1, r"ant\d+\.port$"),
    (CalibrationParams.Lna, 1, r"ant\d+\.lna$"),
    (CalibrationParams.ExtSwCfg, 1, r"ant\d+\.ext_sw_cfg$"),
    (CalibrationParams.AntPaths, 2, r"ant_pair\d+\.ant_paths$"),
    (CalibrationParams.Axis, 1, r"ant_pair\d+\.axis$"),
    (CalibrationParams.PdoaLutId, 1, r"ant_pair\d+\.ch\d+\.pdoa.lut_id$"),
    (CalibrationParams.TxAntPath, 1, r"ant_set\d+\.tx_ant_path$"),
    (CalibrationParams.NbRxAnts, 1, r"ant_set\d+\.nb_rx_ants$"),
    (CalibrationParams.RxAnts, 3, r"ant_set\d+\.rx_ants$"),
    (CalibrationParams.RxAntsArePairs, 1, r"ant_set\d+\.rx_ants_are_pairs$"),
    (CalibrationParams.TxPowerControl, 1, r"ant_set\d+\.tx_power_control$"),
    (CalibrationParams.DebugTxPower, 4, r"debug\.tx_power$"),
    (CalibrationParams.PdoaOffs, 2, r"ant_pair\d+\.ch\d+\.pdoa.axis[xyz].offset$"),
    (CalibrationParams.PdoaOffset, 2, r"ant_pair\d+\.ch\d+\.pdoa.offset$"),
]


for antenna in new_antenna_to_remove:
    CalibrationParams.defs.remove(antenna)


CalibrationParams.defs.extend([
    (CalibrationParams.TxData, 1, r"ant_grp\d+\.rf_config\.tx_data$"),
    (CalibrationParams.TxSts, 1, r"ant_grp\d+\.rf_config\.tx_sts\d+$"),
    (CalibrationParams.Caps, 1, r"ant_grp\d+\.caps$"),
    (CalibrationParams.TxPower, 4, r"debug\.tx_power$"),
    (CalibrationParams.LutId, 1, r"ant_grp\d+\.ch\d+\.pdoa.axis[xyz].lut_id$"),
    (CalibrationParams.AntGrpRfCfgRfOff, 1, r"ant_grp\d+\.rf_config.rfoff$"),
    (CalibrationParams.AntGrpRfCfgTx, 1, r"ant_grp\d+\.rf_config.tx$"),
    (CalibrationParams.AntGrpRfCfgTxAoa, 1, r"ant_grp\d+\.rf_config.tx_aoa$"),
    (CalibrationParams.AntGrpRfCfgRxIp, 1, r"ant_grp\d+\.rf_config.rx_ip$"),
    (CalibrationParams.AntGrpRfCfgRxSts0, 1, r"ant_grp\d+\.rf_config.rx_sts0$"),
    (CalibrationParams.AntGrpRfCfgRxSts1, 1, r"ant_grp\d+\.rf_config.rx_sts1$"),
    (CalibrationParams.AntGrpRfCfgRxSts2, 1, r"ant_grp\d+\.rf_config.rx_sts2$"),
    (CalibrationParams.AntGrpRfCfgRxSts3, 1, r"ant_grp\d+\.rf_config.rx_sts3$"),
    (CalibrationParams.TxPowerControl, 1, r"ant_grp\d+\.tx_power_control$"),
    (CalibrationParams.PdoaOffs, 2, r"ant_grp\d+\.ch\d+\.pdoa.axis[xyz].offset$"),
    (CalibrationParams.AntGrpLnaRxA, 1, r"ant_grp\d+\.lna_rxa$"),
    (CalibrationParams.AntGrpLnaRxB, 1, r"ant_grp\d+\.lna_rxb$"),
    (CalibrationParams.AntGrpPa, 1, r"debug\.pa$"),
    (CalibrationParams.AntGrpStsMode, 1, r"ant_grp\d+\.sts_mode$"),
    (CalibrationParams.AntGrpRxCfg, 1, r"ant_grp\d+\.rx_config$"),
    (CalibrationParams.AntGrpPDoASegments, 6, r"ant_grp\d+\.pdoa_segments$"),
    (CalibrationParams.AntGrpPDoAType, 3, r"ant_grp\d+\.pdoa_type$"),
    (CalibrationParams.AntGrpFixedRfCfgTx, 1, r"ant_grp\d+\.fixed_rf_config.tx$"),
    (CalibrationParams.AntGrpFixedRfCfgRx, 1, r"ant_grp\d+\.fixed_rf_config.rx$"),
    (CalibrationParams.ExtSwConfig, 1, r"ant_grp\d+\.ext_sw_config$"),
    (CalibrationParams.AntGrpCfgMode, 1, r"ant_grp\d+\.rf_config_mode$"),
    (CalibrationParams.AntConfig, 1,  r"ant\d+\.config$"),
    (CalibrationParams.RssiOffSetQ3, 1, r"ant\d+\.ch\d+\.rssi_offset_q3$"),
    (CalibrationParams.ExperimentalMacSession_SchedulerId, 1, r"experimental\.mac\.session_scheduler\.id$")

])




antenna_ids   = [0, 1, 2, 3]
pdoa_lut_ids  = [0, 1, 2, 3]
ref_frame_ids = [0, 1, 2, 3, 4, 5, 6, 7]
cal_params_in = (
        (["xtal_trim", "temperature_reference", 'alternate_pulse_shape'], Uint8),
        (['rf_noise_offset'], Int8),
        (["dual_rx_auto.accum_period", "dual_rx_auto.rssi_diff_thres", "dual_rx_auto.error_rate_thres"], Uint8),
        ([ "debug.pa_enabled"], Uint8),
        ([ "experimental.mac.session_scheduler.id"], Uint8),
        ([ "debug.pll_cfg"], Uint32),
        (["voltage_reference", 'debug.tx_power'], Uint32),
        (["restricted_channels"], Uint16),
        (dot("ch", [5,9],'.pll_locking_code'), Uint8),
        (['wifi_sw_cfg'], Uint8),
        (["wifi_coex_max_grant_duration", "wifi_coex_min_inactive_duration"], Uint8),
        (["wifi_coex_mode", "wifi_coex_time_gap"], Uint8),
        (dot("ch", [5,9], ".wifi_coex_enabled"), Uint8),
        (["rx_diag_config.cir_n_taps", "rx_diag_config.cir_fp_tap_offset"], Uint16),
        (['post_tx.pattern_repetitions'], Uint16),
        (['post_tx.pattern_data'], Uint64),
        (dot("ant", antenna_ids, '.ch', [5, 9], '.ref_frame', ref_frame_ids, '.post_tx_power_index'), Uint32),
        (dot("ant_grp", antenna_ids, '.rf_config.tx_data'), Uint8),
        (dot("ant_grp", antenna_ids, '.rf_config.tx_sts', [1, 2, 3]), Uint8),
        (['debug.rx_segment'], Uint8),
        (dot("ant_grp", antenna_ids, '.caps'), Uint8),

        (dot("ant_grp", antenna_ids, '.tx_power_control'), Uint8),
        (dot("ant", antenna_ids, '.ch', [5, 9], '.ref_frame', ref_frame_ids, '.tx_power_index'), Uint32),
        (dot("ant", antenna_ids, '.ch', [5, 9], '.ref_frame', ref_frame_ids, '.max_gating_gain'), Uint32),
        (dot('ref_frame', ref_frame_ids, '.phy_cfg'), PhyFrame),
        (dot('ref_frame', ref_frame_ids, '.payload_size'), Uint16),
        (dot("ant", antenna_ids, '.ch', [5, 9], '.ant_delay'), Uint32),
        (dot("ant", antenna_ids, '.ch', [5, 9], '.tx_bypass_delay_offset'), Uint8),
        (dot("ant", antenna_ids, '.ch', [5, 9], '.rx_bypass_delay_offset'), Uint8),

        (dot("ant", antenna_ids, '.ch', [5, 9], '.pa_gain_offset'), Int8),
        (dot("ant", antenna_ids, '.ch', [5, 9], '.pg_count'), Uint8),
        (dot("ant", antenna_ids, '.ch', [5, 9], '.pg_delay'), Uint8),
        (dot("ant", antenna_ids, '.ch', [5, 9], '.rssi_offset_q3'), Uint8),

        (dot("ant", antenna_ids, '.config'), AntConf),

        (dot("ant_grp", antenna_ids, '.ch', [5, 9], '.pdoa.axis', ['x', 'y', 'z'], '.lut_id'), Uint8),
        (dot("ant_grp", antenna_ids, '.ch', [5, 9], '.pdoa.axis', ['x', 'y', 'z'], '.offset'), S4_11),
        (dot("pdoa_lut", pdoa_lut_ids, '.data'), AoaTable),

        (dot("ant_grp", antenna_ids, '.rf_config.rfoff'), Uint8),
        (dot("ant_grp", antenna_ids, '.rf_config.tx'), Uint8),
        (dot("ant_grp", antenna_ids, '.rf_config.tx_aoa'), Uint8),
        (dot("ant_grp", antenna_ids, '.rf_config.rx_ip'), Uint8),
        (dot("ant_grp", antenna_ids, '.rf_config.rx_sts',[0,1,2,3]), Uint8),
        (dot("ant_grp", antenna_ids, '.lna_rx',['a','b']), Uint8),
        (dot("ant_grp", antenna_ids, '.rx_config'), Uint8),
        (dot("ant_grp", antenna_ids, '.pdoa_segments'), Uint48),
        (dot("ant_grp", antenna_ids, '.fixed_rf_config', ['.tx', '.rx']), Uint8),
        (dot("ant_grp", antenna_ids, '.ext_sw_config'), Uint8),
        (dot("ant_grp", antenna_ids, '.rf_config_mode'), Uint8),
        (dot("ant_grp", antenna_ids, '.pdoa_type'), Uint24),
        (["ip_sts_sanity_thres_q2"], Uint8),
    )

cal_params.clear()
cal_params.update(build_cal_params(cal_params_in))
