# Copyright (c) 2021 Qorvo US, Inc.

"""
This file is configuring the package depending on the variant used.
A 'variant' is a list of addins and a (customer) customization


# For Qorvo internal repo:

    If UQT_CUSTOM=qorvo all available addins will be loaded.

    It is possible to specify another vendor or reduce the number 
    of addins by setting the below environment variables:
     - UQT_CUSTOM  : qorvo or other customized tag (purple, blue, black, ...)
     - UQT_ADDINS  : a ':' separated list of addins. Default: all
     
    The available customization & addins may be listed using:
     - uqt_lsvariants

    Below convention is used regarding file names:
    - Fira standard is defined in the fira.py (and fira_data.py) file.
    - Qorvo Fira overlay is defined within qorvo.py (and qorvo_data.py) file.
    - Specific customization is defined in custom[__<custom>]
    - Each above file is splitted into different subsystems named:
        - fira__<subsystem>.py
        - qorvo_<subsystem>.py)
        - custom_<subsystem>__<custom>.py
    - Additional features are defined in addins with the following naming: 
      addin_<name>[__<custom>].py
    - when [__<custom>] is existing (watch out: 2 underscores), the addin will only 
      be loaded when the related customer is active through the UQT_CUSTOM env var
    - when [__<custom>] is blank, the addin is loaded whatever UQT_CUSTOM, unless the
      same addin exists with the UQT_CUSTOM tag. 

# For Customer Release:
    - all unwanted files are removed and renamed without the __<custom> suffix
    - This current file is removed and replaced with 
    __init__customer__.py which is configuring the package to load all available files.

    A Customer release is thus a cut-down version of this repo. It contains:
    - __init__.py renamed from __init__customer__.py
    - all files in use ( should we remove __<custom> ? tbd)
"""

from glob import glob
from os import getenv
from os.path import dirname, basename, exists, getsize


# Get uci path:
import uci.fira
uci_path=dirname(uci.fira.__file__)

# Find available vendor and addins:
available_customs=['qorvo', 'purple', 'radar', 'SD2002', 'qm33sdk','other']
available_addins=[]
for m_path in glob(f'{uci_path}/addin_*.py'):
    m_name=basename(m_path)
    m_name = m_name.replace('.py','')
    available_addins.append(m_name)
    
# Find wanted vendor and addins:
customization = getenv('UQT_CUSTOM')
if customization is None:
    raise Exception("Missing customization variable (UQT_CUSTOM) in the environment")

addins=getenv('UQT_ADDINS', 'all')
if addins=='all':
    addins=available_addins
else:
    addins=addins.split(':')

# Import std:
from .fira import *

# Import Qorvo:
from .qorvo import *

# Import Customization:
if (customization=='qorvo') or not exists(f'{uci_path}/custom__{customization}.py'):
    from .custom import *
else:
    exec(f'from .custom__{customization} import *')

# Import addins, filtering on wanted customization:
loaded_addins=[]
for addin in addins:
    if addin == '':
        continue
    name, *cust=addin.split('__')
    if (cust == []) and (exists(uci_path + '/' + name + '__' + customization + '.py') \
            or exists(uci_path + '/' + name + '__customer' + '.py')): continue
    if (cust != []) and (cust[0] != customization) and (cust[0] != 'customer') : continue
    #  filter-out empty files (less that 200 bytes) used to black-list a default addins:
    if getsize(f'{uci_path}/{addin}.py') <200 : continue 

    try:
        exec(f'from .{addin} import *')
        loaded_addins.append(addin)
    except Exception as e:
        raise Exception(f'Unable to import ".{addin}" : {e!r}')
