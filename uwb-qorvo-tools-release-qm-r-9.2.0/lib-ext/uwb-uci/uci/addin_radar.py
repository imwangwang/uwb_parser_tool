#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

# Do not put in __all__ your uci Client, new Gids, or other extension objects
#  unless you want to block the addin mechanism.

__all__ = []

import math
import queue
import struct
from collections import deque
import datetime
import json
import queue
from collections import deque

from uci.utils import DynIntEnum
from uci.fira import App, SessionType, SessionStateChangeReason, OidRanging

class RadarSessionTypeExtension(DynIntEnum):
    Radar = 0xA1

SessionType.extend(RadarSessionTypeExtension)

class RadarAppExtension(DynIntEnum):
    TimingParams = 0xB0
    SamplesPerSweep = 0xB1
    SweepOffset = 0xB2
    BitsPerSample = 0xB3
    NumberOfBursts = 0xB4
    RadarDataType= 0xB5
    AntennaSetId = 0xB6
    TxProfileIdx = 0xB7

App.extend(RadarAppExtension)

App.defs.extend([
    (App.TimingParams, [7, [4, 2, 1]]),
    (App.SamplesPerSweep, 1),
    (App.SweepOffset, 2),
    (App.BitsPerSample, 1),
    (App.NumberOfBursts, 2),
    (App.RadarDataType, 1),
    (App.AntennaSetId, 1),
    (App.TxProfileIdx, 1)
    ])

class RadarOidRangingExtension(DynIntEnum):
    Radar = 0x0a

OidRanging.extend(RadarOidRangingExtension)
