#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc

All rights reserved.

NOTICE: All information contained herein is, and remains the property
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and
may be covered by patents, patent applications, and are protected by
trade secret and/or copyright law. Dissemination of this information
or reproduction of this material is strictly forbidden unless prior written
permission is obtained from Qorvo, Inc.
"""

import weakref
import logging
import os
import select
import socket
import threading
import subprocess
from abc import ABCMeta, abstractmethod
import re

import serial
import serial.threaded
import serial.tools.list_ports
from uci.utils import UciComStatus, UciComError

logger = logging.getLogger(__name__)


class Factory(ABCMeta):
    __transports__ = []

    def __init__(cls, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if len(cls.mro()) > 2:
            cls.register()

    def __del__(cls):
        if len(cls.mro()) > 2:
            cls.unregister()

    def register(cls):
        if cls in Factory.__transports__:
            raise ValueError(f'{cls} already registered')
        Factory.__transports__.append(cls)

    def unregister(cls):
        if cls not in Factory.__transports__:
            raise ValueError(f'{cls} not registered')
        Factory.__transports__.remove(cls)

    @staticmethod
    def get(callback, *args, **kwargs):
        port = kwargs['port']

        for tr in Factory.__transports__:
            if tr.handle(port):
                return tr(callback, *args, **kwargs)

        raise UciComError(UciComStatus.UnknownPort, f'"{port}" is not supported')


class ITransport(metaclass=Factory):
    """Abstract class for Transport. To define a new transport one needs
    to create a class heriting from it."""
    @abstractmethod
    def __init__(self, callback, *args, **kwargs):
        """A transport must provide a callback parameter to __init__.  The
        callback is used to receive an UCI packet like: def
        callback(packet):
            pass
        """
        raise NotImplementedError

    @abstractmethod
    def write(self, packet):
        """Write a UCI packet using the transport."""
        raise NotImplementedError

    @abstractmethod
    def close(self):
        """Close the transport."""
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def handle(port):
        """Static method used by the transport to tell the factory it can
        handle 'port' transport."""
        raise NotImplementedError


class UartTransportProtocol(serial.threaded.Protocol):
    def __init__(self, callback):
        self.transport = None
        self.buffer = bytearray()
        self.cb = callback
        self.is_synchronized=False

    def connection_made(self, tr):
        self.transport = tr

    def connection_lost(self, exc):
        self.transport = None
        self.buffer.clear()

    def check_data(self):
        while True:
            n = len(self.buffer)
            if n < 4:
                return

            mt = (self.buffer[0] & 0xe0) >> 5 

            if mt in [0b001, 0b010, 0b011]:
                # Control Packet
                size = self.buffer[3]
            elif mt in [0b000]:
                # Data Packet
                size = self.buffer[2] | self.buffer[3] << 8
            elif mt in [0b100, 0b101]:
                # Control message format for testing
                size = self.buffer[2] | self.buffer[3] << 8
            else:
               # Error -> flush data
                self.buffer = bytearray()
                size = 0
                return

            if n < 4 + size:
                return

            # got a packet
            self.cb()(self.buffer[0:4+size])

            self.buffer = self.buffer[4+size:]

    def data_received(self, data):
        #logger.debug(f'recv bytes : {data.hex(".")}')
        if not self.is_synchronized:
            # Just arriving in the com!
            if (data[0] & 0xf0)>>4 in [4,6,5,7]:
                self.is_synchronized=True
            else:
                # We are in the middle of a byte stream...
                logger.debug(f'recv bytes purged: {data.hex(".")}')
                data=b''
        self.buffer.extend(data)

        self.check_data()


class UartTransport(serial.threaded.ReaderThread, ITransport):
    def __init__(self, callback, *args, **kwargs):
        # default to 115200
        if 'baudrate' not in kwargs:
            kwargs['baudrate'] = 115200
        if kwargs['port'].startswith('uart:'):
            kwargs['port']=kwargs['port'][5:]
        if 'url' not in kwargs:
            kwargs['url'] = kwargs.pop('port')

        super().__init__(serial.serial_for_url(*args, **kwargs),
                         lambda: UartTransportProtocol(callback))

        self.start()
        self.connect()

    def __del__(self):
        if os.name !='nt':
            self.serial.cancel_read()
            self.close()

    @staticmethod
    def handle(port):
        if os.path.islink(port):
            # handle links in /dev
            port = os.path.realpath(port)

        return port.startswith('uart:') or port in [x.device for x in serial.tools.list_ports.grep(port)]


class DevTransport(ITransport):
    def __init__(self, callback, *args, **kwargs):
        self.device = os.open(kwargs['port'], os.O_RDWR)
        self.cb = callback

        (self.rpipe, self.wpipe) = os.pipe()

        self.reader_thread = threading.Thread(
            target=self.reader_fn, daemon=True)
        self.reader_thread.start()

    def reader_fn(self):
        poller = select.poll()

        poller.register(self.rpipe, select.POLLIN)
        poller.register(self.device, select.POLLIN)

        while True:
            for (fd, _) in poller.poll():
                if fd == self.rpipe:
                    return

                packet = os.read(fd, 4 + 255)
                self.cb()(packet)

    def write(self, packet):
        os.write(self.device, packet)

    def close(self):
        os.close(self.rpipe)
        os.close(self.wpipe)
        self.reader_thread.join()
        os.close(self.device)

    @staticmethod
    def handle(port):
        return port.startswith('/dev/uci')

class TcpTransport(ITransport):
    """
    Can use "socat -x TCP4-LISTEN:1234,fork,reuseaddr /dev/uci0" as a server.
    """
    def __init__(self, callback, *args, **kwargs):
        # port is tcp:<ip>:<port>
        host, port = kwargs['port'].split(":", 1)[1].rsplit(":", 1)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((host, int(port)))
        self.cb = callback

        (self.rpipe, self.wpipe) = os.pipe()

        self.reader_thread = threading.Thread(
            target=self.reader_fn, daemon=True)
        self.reader_thread.start()

    def reader_fn(self):
        poller = select.poll()

        poller.register(self.rpipe, select.POLLIN)
        poller.register(self.sock.fileno(), select.POLLIN)

        while True:
            for (fd, _) in poller.poll():
                if fd == self.rpipe:
                    return

                packet = self.sock.recv(4 + 255)
                self.cb()(packet)

    def write(self, packet):
        self.sock.sendall(packet)

    def close(self):
        os.close(self.rpipe)
        os.close(self.wpipe)
        self.reader_thread.join()
        self.sock.close()

    @staticmethod
    def handle(port):
        return port.startswith('tcp:')

def to_str(packet):
    return ''.join([format(x, '02x') for x in packet])

class AdbPollTransport(ITransport):

    def __init__(self, callback, *args, **kwargs):
        port=kwargs['port']
        # port.startswith('adb') may be:
        # adb                                          -> USB, single device
        # adb:<serial number>  ie adb:6b70d4cf0506     -> USB, several device
        # adb:<ip>:<port>      ie adb:10.42.0.21:38339 -> TCP
        if port.startswith('adb:'):
            android_serial=kwargs['port'][4:]
            app = ["adb", "-s", android_serial, "shell", "-x", "/vendor/bin/factory/qm35_poll"]
            subprocess.run(["adb", "-s", android_serial, "shell", "stop", "vendor.uwb_hal"])
            subprocess.run(["adb", "-s", android_serial, "shell", "stop", "qorvo.uci-daemon"])
            subprocess.run(["adb", "-s", android_serial, "shell", "killall", "qm35_poll"], stderr=subprocess.DEVNULL)
        else:
            app = ["adb", "shell", "-x", "/vendor/bin/factory/qm35_poll"]
            subprocess.run(["adb", "shell", "stop", "vendor.uwb_hal"])
            subprocess.run(["adb", "shell", "stop", "qorvo.uci-daemon"])
            subprocess.run(["adb", "shell", "killall", "qm35_poll"], stderr=subprocess.DEVNULL)
        logger.debug(f'using: {app}')
        # todo: verify if other process to stop ?
        self.proc = subprocess.Popen(app, stdout=subprocess.PIPE, stdin=subprocess.PIPE, bufsize=0, text=True)
        # self.proc.stdout.readall() # purging not needed ?
        self.cb = callback
        self.re_err=re.compile('^[^0-9,a-f,A-F]+')
        self.re_nohex=re.compile('[^0-9,a-f,A-F]+')
        self.reader_thread = threading.Thread(target=self.reader_fn, daemon=True)
        self.reader_thread.start()

    def reader_fn(self):
        self.running = True
        while self.running:
            packet = self.proc.stdout.readline().strip()
            if packet=='':
                continue
            if  self.re_err.search(packet):
                # We are dealing with an adb/android/qm35_poll message...
                logger.warn(f'qm35_poll <-- {repr(packet)} (android message)')
                continue
            if self.re_nohex.search(packet):
                logger.warn(f'qm35_poll <-- noisy:"{repr(packet)}"')
                # We have unexpected wrong bytes in the message
                # let's suppose it is just additional garbage bytes and drop them...
                while m:=self.re_nohex.search(packet):
                    logger.warning(f'qm35_poll : dropping "{m.group(0)}"')
                    packet=packet[:m.start(0)]+packet[m.end(0):]
            if packet.startswith(('4','6', '5','7')) :
                logger.debug(f'qm35_poll <-- {repr(packet)} (resp/ntf)')
                # We are dealing with:
                # - a response message (4 or 5 if BPF=1)
                # - a NTF message (6 or 7 if BPF=1)
                try:
                    self.cb()(bytes.fromhex(packet))
                except ValueError:
                    logger.warning(f'qm35_poll : wrong UCI packet received. {repr(packet)}')
            elif packet.startswith(('2')) :
                logger.debug(f'qm35_poll <-- {repr(packet)} (echo)')
            else:
                logger.warning(f'qm35_poll <-- {repr(packet)} (unexpected) ')

    def write(self, packet):
        packet_str = packet.hex()+ '\n'
        logger.debug(f'qm35_poll --> {repr(packet_str)} (req)')
        try:
            self.proc.stdin.write(packet_str)
        except BrokenPipeError as e:
            raise UciComError(UciComStatus.Error, e)

    def __del__(self):
        self.close()

    def close(self):
        self.proc.terminate()
        self.running = False
        self.reader_thread.join(timeout=1)

    @staticmethod
    def handle(port):
        return port.startswith('adb')

try:
    import uci.hsspi as hsspi
except ImportError:
    logger.debug('HSSPITransport not registered')
else:
    class HSSPITransport(hsspi.HsspiTransportProtocol, ITransport):
        def __init__(self, callback, *args, **kwargs):
            if kwargs['port'].startswith('hsspi:'):
                kwargs['port']=kwargs['port'][6:]
            super().__init__(*args, **kwargs)

            self.set_msg_handler(callback)
            self.start()

        def write(self, packet):
            super().write(hsspi.UL_UCI, packet)

        @staticmethod
        def handle(port):
            return port[:7] == 'ftdi://' or port.startswith('hsspi:')

try:
    import uci.hsspi_transport_v2 as hsspi_v2
except ImportError:
    logger.debug('HSSPITransport_v2 not registered')
else:
    class HSSPITransportV2(hsspi_v2.HsspiTransportProtocolV2, ITransport):
        def __init__(self, callback, *args, **kwargs):
            kwargs['port'] = kwargs['port'][9:]
            super().__init__(*args, **kwargs)

            self.set_msg_handler(callback)
            self.start()

        def write(self, packet):
            super().write(hsspi_v2.UL_UCI, packet)

        @staticmethod
        def handle(port):
            return port.startswith('hsspi_v2:')
