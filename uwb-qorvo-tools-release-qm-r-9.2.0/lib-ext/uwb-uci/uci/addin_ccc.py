#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (c) 2020-2023 Qorvo, Inc
 
All rights reserved. 
 
NOTICE: All information contained herein is, and remains the property 
of Qorvo, Inc. and its suppliers, if any. The intellectual and technical 
concepts herein are proprietary to Qorvo, Inc. and its suppliers, and 
may be covered by patents, patent applications, and are protected by 
trade secret and/or copyright law. Dissemination of this information 
or reproduction of this material is strictly forbidden unless prior written 
permission is obtained from Qorvo, Inc. 
"""

# Do not put in __all__ your uci Client, new Gids, or other extension objects 
# unless you want to block the addin mechanism.

__all__ = []

from uci.utils import DynIntEnum
import uci.fira as fira
import uci.qorvo as qorvo


class SessionType_extension(DynIntEnum):
    CccRanging = 0xA0
    AliroRanging = 0xA2


fira.SessionType.extend(SessionType_extension)


class OidQorvo_extension(DynIntEnum):
    CccSetAntennaFlex = 0x0A  # CCC_SET_ANT_FLEX_CONFIG_CMD/RSP
    CccGetAntennaFlex = 0x0B  # CCC_GET_ANT_FLEX_CONFIG_CMD/RSP


qorvo.OidQorvo.extend(OidQorvo_extension)


class Client_extension():
    def get_antenna_conf_ccc(self, params):
        """params: liste of integers expected: chan_1, param_idx_1, chan_2, param_idx_2, ..."""
        params = [len(params) // 2] + params
        payload = self.command(fira.Gid.Qorvo, qorvo.OidQorvo.CccGetAntennaFlex, bytes(params))
        return qorvo.AntennaFlexParams(payload)

    def set_antenna_conf_ccc(self, params):
        """params: liste of integers expected: chan_1, param_idx_1, value_1, chan_2, param_idx_2, value_2,..."""
        params = [len(params) // 3] + params
        payload = self.command(fira.Gid.Qorvo, qorvo.OidQorvo.CccSetAntennaFlex, bytes(params))
        return fira.Status((int).from_bytes(payload, 'little'))


fira.Client.extend(Client_extension)

# =============================================================================
# UCI Message Extension
# =============================================================================

for i in (qorvo.OidQorvo.CccSetAntennaFlex, qorvo.OidQorvo.CccGetAntennaFlex):
    fira.uci_codecs[(fira.MT.Command, fira.Gid.Qorvo, i)] = fira.default_codec(i.name)
    fira.uci_codecs[(fira.MT.Response, fira.Gid.Qorvo, i)] = fira.default_codec(i.name)

fira.uci_codecs.update({
    (fira.MT.Response, qorvo.Gid.Qorvo, qorvo.OidQorvo.CccGetAntennaFlex): qorvo.AntennaFlexParams
})
