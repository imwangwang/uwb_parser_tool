import json
import numpy as np
import matplotlib.pyplot as plt

# Load the JSON data from the file again
with open("./session_logs/session_42_240624_1450.json", "r") as file:
    data = json.load(file)

# Extracting timestamp, real (I), and imaginary (Q) components again
timestamps = []
amplitudes = []

for entry in data:
    for sweep in entry['sweepData']:
        timestamp = sweep['timestamp']
        samples = sweep['samples']
        for sample in samples:
            # Sample is in the format "(real, imaginary)"
            real, imag = map(int, sample.strip('()').split(','))
            amplitude = np.sqrt(real**2 + imag**2)
            timestamps.append(timestamp)
            amplitudes.append(amplitude)

# Convert timestamps and amplitudes to numpy arrays for better handling
timestamps = np.array(timestamps)
amplitudes = np.array(amplitudes)

# Normalize timestamps to start from zero
timestamps -= timestamps[0]

# Plotting the amplitude vs time
plt.figure(figsize=(12, 6))
plt.plot(timestamps, amplitudes, label='Amplitude')
plt.xlabel('Time (ns)')
plt.ylabel('Amplitude')
plt.title('Amplitude of CIR samples over Time')
plt.legend()
plt.grid(True)
plt.show()
